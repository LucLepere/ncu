#!/usr/bin/env python

from netaddr import *

import time, socket
import sys, os, subprocess, re
from subprocess import call
from optparse import OptionParser

DEBUG_MODE = False
DEFAULT_CONGESTION_CONTROL = "reno"
CAPS_FOLDER = "/hosthome/Documents/wireshark_data"
CLICK_EXECUTABLE = "/hosthome/nk_ncu/click"
CLICK_CONFIG_GENERATOR = "/hosthome/nk_ncu/make-ip-conf-custom.pl"
CLICK_CONFIG_TARGET = "/tmp/ip_router_config.click"
QUEUE_MANAGER = "/hosthome/nk_ncu/queue_manager.py"

verbose = False


# ==============================================================================
#                                   FUNCTONS
# ==============================================================================

# Exec command
def cmd(command) :
    global verbose
    if verbose : print command
    if DEBUG_MODE : call(command, shell=True, stdout=open(os.devnull, "w"))
    else : call(command, shell=True, stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"))

# Exec command and get output
def cmd_output(command) :
    output = subprocess.Popen(command.split(" "), stdout=subprocess.PIPE).communicate()[0]
    if verbose :
        print command
        print '> '+output
    return output

def list_interfaces() :
    output = subprocess.Popen(['ifconfig', '-s'], stdout=subprocess.PIPE).communicate()[0]
    output = output.rstrip().split("\n")
    output.pop(0)
    interfaces = []
    for line in output :
        interfaces.append(line.split(" ").pop(0))
    return interfaces

def list_congestion_algos() :
    avail = cmd_output("sysctl net.ipv4.tcp_available_congestion_control").strip()
    avail = avail.split(" ")
    avail.reverse()
    avail.pop()
    avail.pop()

    cmd = "ls /lib/modules/"+cmd_output("uname -r").strip()+"/kernel/net/ipv4/"
    others = cmd_output(cmd).split("\n")

    others2 = []
    for o in others :
        if o.startswith("tcp_") :
            others2.append(o.strip().replace("tcp_", "").replace(".ko", ""))

    l = list(set(avail+others2))
    l.sort()
    return l


# ==============================================================================
#                                   MAIN
# ==============================================================================

def main():
    global verbose

	# Must be root (because of tc, sysctl, ...)
    if not os.geteuid() == 0 :
        sys.exit("Only root can run this script")

    usage = "Usage: %prog [options]"
    parser = OptionParser(usage=usage)


    # ==============================================================================
    #                                   OPTIONS DEFINITION
    # ==============================================================================

    parser.add_option(
    	'-a',
    	'--all',
    	action="store_true",
    	dest="all",
    	help='show all current variable values')
    parser.add_option(
    	'-d',
    	'--default',
    	action="store_true",
    	dest="default",
    	help='put default values')
    parser.add_option(
    	'-v',
    	'--verbose',
    	action="store_true",
    	dest="verbose",
    	help='verbose (show used commands)')
    parser.add_option(
    	'-w',
    	'--wireshark',
    	dest="interface",
    	help='launch Wireshark on selected interface')
    parser.add_option(
    	'-b',
    	'--bandwidth',
    	dest="interface_bandwidth",
    	nargs=2,
    	metavar="INTERFACE BANDWIDTH",
    	help='change upload bandwidth of an interface and network emulation values ; example of use : easychange.py -b eth0 rate=10mbit,delay=20ms,loss=5%,duplicate=1%,reorder=25% ; just separate the values with commas and omit what you don\'t want to use')
    parser.add_option(
        '-g',
        '--gateway',
        dest="gateway",
        metavar="IP",
        help='add default route to gateway')
    parser.add_option(
    	'-c',
    	'--congestion',
    	dest="algorithm",
    	help='change congestion control algorithm')
    parser.add_option(
    	'-q',
    	'--queue',
    	action="store_true",
    	dest="queue",
    	help='launch Click IP router with a managed queue')
    parser.add_option(
    	'-s',
    	'--stop',
    	action="store_true",
    	dest="stop_click",
    	help='stop Click IP router')
    parser.add_option(
    	'-l',
    	'--list',
    	action="store_true",
    	dest="list",
    	help='show available congestion control algorithms')
    parser.add_option(
    	'--f4',
    	action="store_true",
    	dest="forward4",
    	help='toggle IPv4 forwarding')
    parser.add_option(
    	'--f6',
    	action="store_true",
    	dest="forward6",
    	help='toggle IPv6 forwarding')

    # Handle options
    (options, args) = parser.parse_args()
    verbose = options.verbose
    
    if (DEBUG_MODE) : verbose = True


    # ==============================================================================
    #                                   HANDLING OPTIONS
    # ==============================================================================



    # Put back default values
    if (options.default) :

    	# congestion control algorithm
        cmd("sysctl -w net.ipv4.tcp_congestion_control="+DEFAULT_CONGESTION_CONTROL)

        # traffic shaping
        for interface in list_interfaces() :
            cmd("orig-tc qdisc del dev "+interface+" root")

        # ip forwarding
        # v4
        cmd("sysctl -w net.ipv4.ip_forward=1")
        # v6
        cmd("sysctl -w net.ipv6.conf.all.forwarding=0")


    # Show all current values
    if (options.all) :
        print "-----"

        # congestion control algorithm
        call("sysctl net.ipv4.tcp_congestion_control", shell=True) # call instead of cmd intentionally
        print "-----"

        # traffic shaping
        for interface in list_interfaces() :
            print interface+" :"
            call("orig-tc qdisc show dev "+interface, shell=True) # call instead of cmd intentionally
            call("orig-tc class show dev "+interface, shell=True) # call instead of cmd intentionally
            print "-----"

        # ip forwarding
        # v4
        call("sysctl net.ipv4.ip_forward", shell=True) # call instead of cmd intentionally
        # v6
        call("sysctl net.ipv6.conf.all.forwarding", shell=True) # call instead of cmd intentionally
        print "-----"



    # Show available congestion control algorithms
    if (options.list) :
        print "Available congestion control algorithms :"
        for a in list_congestion_algos() :
            if (a == DEFAULT_CONGESTION_CONTROL) :
                print "\t"+a+" (default)"
            else :
                print "\t"+a



    # Change congestion control algorithm
    if (options.algorithm) :

    	# Check arg congestion algorithm
        if (options.algorithm not in list_congestion_algos()) :
            print 'Error : '+options.algorithm+' is not an available congestion control algorithm'
            sys.exit(1)

    	# Change congestion control algorithm
        cmd("sysctl -w net.ipv4.tcp_congestion_control="+options.algorithm)



    # Add default route to gateway
    if (options.gateway) :

        # Check arg ip
        gw = ""
        try :
            gw = IPAddress(options.gateway)
        except AddrFormatError :
            print 'Error : '+options.gateway+' is not a valid IP address'
            sys.exit(1)

        # Add default route to this gateway
        if (gw.version == 6) :
            cmd("route -A inet6 add default gw "+options.gateway)
        else :
            cmd("route add default gw "+options.gateway)



    # Launch Click managed queue and its external manager
    if (options.queue) :

        # Check if enough running interfaces
        interfaces = list_interfaces()
        if (len(interfaces) == 0 or (len(interfaces) == 1 and interfaces[0]=="lo")) :
            print("Error : none available interface")
            sys.exit(1)

        # Call Perl script to generate .click configuration file
        cmd("perl "+CLICK_CONFIG_GENERATOR+ " > "+CLICK_CONFIG_TARGET)

        # Kill possible running Click
        cmd("pkill click")

        # Check if forlwaing v6
        ip_forwarding = cmd_output("sysctl net.ipv6.conf.all.forwarding")
        fw_v6_before = False
        if (ip_forwarding.strip().endswith("1")) :
            fw_v6_before = True

        # Disable IP forwarding
        print("Disabling IP forwarding...")
        cmd("sysctl -w net.ipv4.ip_forward=0")
        cmd("sysctl -w net.ipv6.conf.all.forwarding=0")

        # Launch Click with config file
        print("Launching Click in background...")
        cmd(CLICK_EXECUTABLE+" "+CLICK_CONFIG_TARGET+" &")
        # Let some time for Click to initialize
        time.sleep(1)

        # Launch queue manager
        os.system(QUEUE_MANAGER)

        # Kill Click and out back ip forwarding
        print("\nStopping Click...")
        cmd("pkill click")
        print("Re-enabling IP forwarding...")
        cmd("sysctl -w net.ipv4.ip_forward=1")
        if (fw_v6_before) :
            cmd("sysctl -w net.ipv6.conf.all.forwarding=1")



    # Change traffic shaping (tbf rate + netem) on an interface
    if (options.interface_bandwidth) :

        # Check arg interface
        if (options.interface_bandwidth[0] not in list_interfaces()) :
            print 'Error : '+options.interface_bandwidth[0]+' is not a valid interface'
            sys.exit(1)
        if_str = options.interface_bandwidth[0]

        # Delete old shaping
        cmd("orig-tc qdisc del dev "+if_str+" root")

        shaping = options.interface_bandwidth[1].split(',')

        rate = None
        for s in shaping :
            if (s.startswith('rate')) :
                rate = s

        if (rate != None) :
            rate_str = rate.split('=')[1]
            cmd("orig-tc qdisc add dev "+if_str+" root handle 1:0 tbf rate "+rate_str+" burst 10kb latency 60ms")
            shaping.remove(rate)

        netem_str = ' '.join(shaping).replace('=', ' ')

        if (len(shaping) >= 1) :
            if (rate != None) :
                cmd("orig-tc qdisc add dev "+if_str+" parent 1:1 handle 10: netem "+netem_str)
            else :
                cmd("orig-tc qdisc add dev "+if_str+" root netem "+netem_str)   



    # Toggle ip_forward (v4)
    if (options.forward4) :

        ip_forwarding = cmd_output("sysctl net.ipv4.ip_forward")
        if (ip_forwarding.strip().endswith("1")) :
            cmd("sysctl -w net.ipv4.ip_forward=0")
        else :
            cmd("sysctl -w net.ipv4.ip_forward=1")



    # Toggle ip_forward (v6)
    if (options.forward6) :

        ip_forwarding = cmd_output("sysctl net.ipv6.conf.all.forwarding")
        if (ip_forwarding.strip().endswith("1")) :
            cmd("sysctl -w net.ipv6.conf.all.forwarding=0")
        else :
            cmd("sysctl -w net.ipv6.conf.all.forwarding=1")



    # Launch Wireshark on host for an interface 
    if (options.interface) :

        # Check arg interface
        if (options.interface not in list_interfaces()) :
            print 'Error : '+options.interface+' is not a valid interface'
            sys.exit(1)

        # Compute filename
        host = socket.gethostname()
        date_hour = time.strftime('%d-%m_%H:%M:%S',time.localtime())
        filename = host+"_"+options.interface+"_"+date_hour+".cap"

        # Check if by extraordinary file would already exist
        filepath = CAPS_FOLDER+"/"+filename
        if (os.path.isfile(filepath)) :
            filepath_wo_ext = filepath[:-4]
            i = 1
            while (os.path.isfile(filepath_wo_ext+"("+str(i)+").cap")) :
                i += 1
            filepath = filepath_wo_ext+"("+str(i)+").cap"

        cmd("tcpdump -n -U -w "+filepath+" -i "+options.interface)



if __name__ == "__main__" :
    sys.exit(main())


