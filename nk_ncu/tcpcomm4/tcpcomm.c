#include "tcpcomm.h"

// Check if an ip address is valid
int is_valid_ip_address(char *ip_address)
{
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ip_address, &(sa.sin_addr));
    return result != 0;
}

// Calculate difference between two timevals
double time_to_seconds(struct timeval *tstart, struct timeval *tfinish)
{
    return (tfinish->tv_sec - tstart->tv_sec) + (tfinish->tv_usec - tstart->tv_usec) / 1e6;
	//return (tfinish->tv_sec) + (tfinish->tv_usec) / 1e6;
}

// Prepare LiveGraph data file settings with filename
void prepare_lgdfs(char *filepath)
{
    char cmd[FILEPATH_LENGTH];
    // "sed" command provides a string replacement in a file in place with regex
    sprintf(cmd, "sed -i -e 's#<entry key=\"DataFile\">.*</entry>#<entry key=\"DataFile\">%s</entry>#' %s", filepath, LGDFS_FILE);
    system(cmd);
}

// Format header string based on tcpi variables numbers
void format_str_header(char *str, char *vars_nums, int nb_pos)
{
    if (vars_nums != NULL)
    {
        char *token;
        char buf[100];
        int pos_i = 1;

        // Add time column
        strcat(str, "time,");

        while (pos_i <= nb_pos) {
            char *vars_cpy = malloc(sizeof(char) * strlen(vars_nums));
            strcpy(vars_cpy, vars_nums);
            token = strtok(vars_cpy,"+");
            while (token != NULL)
            {
                char *var_name = get_tcpi_var_name(strtol(token, NULL, 10));
                // Indicate to which position the variable belongs if relevant
                if (nb_pos == 1) sprintf(buf, "%s", var_name);
                else sprintf(buf, "[%d]%s", pos_i, var_name);
                strcat(str, buf);
                token = strtok(NULL, "+");
                if (token != NULL || pos_i < nb_pos) {
                    strcat(str, ",");
                }
            }
            free(vars_cpy);
            pos_i++;
        }
    }
}

// Format string with values of tcpi variables
void format_str_vars(char *str, struct tcp_info *tcp_info, char *vars_nums, int pos, int nb_pos, struct timeval *tstart)
{
    if (vars_nums != NULL)
    {
        int pos_i = 1;
        char *token;
        char buf[100];

        // Put time in microseconds
        struct timeval tnow;
        if (gettimeofday(&tnow, NULL) != 0)
        {
            fprintf(stderr, "Cannot get time (gettimeofday failed)\n");
        }
        sprintf(buf, "%f,", time_to_seconds(tstart, &tnow));
        strcat(str, buf);

        while (pos_i <= nb_pos) {
            char *vars_cpy = malloc(sizeof(char) * strlen(vars_nums));
            strcpy(vars_cpy, vars_nums);
            token = strtok(vars_cpy,"+");
            while (token != NULL)
            {
                // Put variable value if column relative to this position, the others stay empty
                if (pos == pos_i) {
                    sprintf(buf, "%d", get_tcpi_var(tcp_info, strtol(token, NULL, 10)));
                    strcat(str, buf);
                }
                token = strtok(NULL, "+");
                if (token != NULL || pos_i < nb_pos) {
                    strcat(str, ",");
                }
            }
            free(vars_cpy);
            pos_i++;
        }
        sprintf(buf, "\n");
        strcat(str, buf);
    }
}

// Return tcpi variable value based on its number
int get_tcpi_var(struct tcp_info *tcp_info, int n)
{
    switch(n)
    {
    case 1 :  return tcp_info->tcpi_state;        break;
    case 2 :  return tcp_info->tcpi_ca_state;     break;
    case 3 :  return tcp_info->tcpi_retransmits;  break;
    case 4 :  return tcp_info->tcpi_probes;       break;
    case 5 :  return tcp_info->tcpi_backoff;      break;
    case 6 :  return tcp_info->tcpi_options;      break;
    case 7 :  return tcp_info->tcpi_snd_wscale;   break;
    case 8 :  return tcp_info->tcpi_rcv_wscale;   break;

    case 9 :  return tcp_info->tcpi_rto;          break;
    case 10 : return tcp_info->tcpi_ato;          break;
    case 11 : return tcp_info->tcpi_snd_mss;      break;
    case 12 : return tcp_info->tcpi_rcv_mss;      break;

    case 13 : return tcp_info->tcpi_unacked;      break;
    case 14 : return tcp_info->tcpi_sacked;       break;
    case 15 : return tcp_info->tcpi_lost;         break;
    case 16 : return tcp_info->tcpi_retrans;      break;
    case 17 : return tcp_info->tcpi_fackets;      break;

    // Times
    case 18 : return tcp_info->tcpi_last_data_sent; break;
    case 19 : return tcp_info->tcpi_last_ack_sent;  break;
    case 20 : return tcp_info->tcpi_last_data_recv; break;
    case 21 : return tcp_info->tcpi_last_ack_recv;  break;

    // Metrics
    case 22 : return tcp_info->tcpi_pmtu;         break;
    case 23 : return tcp_info->tcpi_rcv_ssthresh; break;
    case 24 : return tcp_info->tcpi_rtt;          break;
    case 25 : return tcp_info->tcpi_rttvar;       break;
    case 26 : return tcp_info->tcpi_snd_ssthresh; break;
    case 27 : return tcp_info->tcpi_snd_cwnd;     break;
    case 28 : return tcp_info->tcpi_advmss;       break;
    case 29 : return tcp_info->tcpi_reordering;   break;

    case 30 : return tcp_info->tcpi_rcv_rtt;      break;
    case 31 : return tcp_info->tcpi_rcv_space;    break;

    case 32 : return tcp_info->tcpi_total_retrans; break;
    }
    return 0;
}

// Return tcpi variable name based on its number
char* get_tcpi_var_name(int n)
{
    switch(n)
    {
    case 1 :  return "state";        break;
    case 2 :  return "ca_state";     break;
    case 3 :  return "retransmits";  break;
    case 4 :  return "probes";       break;
    case 5 :  return "backoff";      break;
    case 6 :  return "options";      break;
    case 7 :  return "snd_wscale";   break;
    case 8 :  return "rcv_wscale";   break;

    case 9 :  return "rto";          break;
    case 10 : return "ato";          break;
    case 11 : return "snd_mss";      break;
    case 12 : return "rcv_mss";      break;

    case 13 : return "unacked";      break;
    case 14 : return "sacked";       break;
    case 15 : return "lost";         break;
    case 16 : return "retrans";      break;
    case 17 : return "fackets";      break;

    // Times
    case 18 : return "last_data_sent"; break;
    case 19 : return "last_ack_sent";  break;
    case 20 : return "last_data_recv"; break;
    case 21 : return "last_ack_recv";  break;

    // Metrics
    case 22 : return "pmtu";         break;
    case 23 : return "rcv_ssthresh"; break;
    case 24 : return "rtt";          break;
    case 25 : return "rttvar";       break;
    case 26 : return "snd_ssthresh"; break;
    case 27 : return "snd_cwnd";     break;
    case 28 : return "advmss";       break;
    case 29 : return "reordering";   break;

    case 30 : return "rcv_rtt";      break;
    case 31 : return "rcv_space";    break;

    case 32 : return "total_retrans"; break;
    }
    return 0;
}

// Print all tcpi variables with their name and number
void show_vars()
{
    printf("\n TCP_info variables with their number (used with -v option)\n\n \
1 :  state\n \
2 :  ca_state \n \
3 :  retransmits  \n \
4 :  probes   \n \
5 :  backoff  \n \
6 :  options  \n \
7 :  snd_wscale   \n \
8 :  rcv_wscale   \n \
\n \
9 :  rto  \n \
10 : ato  \n \
11 : snd_mss  \n \
12 : rcv_mss  \n \
\n \
13 : unacked  \n \
14 : sacked   \n \
15 : lost \n \
16 : retrans  \n \
17 : fackets  \n \
\n \
Times\n \
18 : last_data_sent \n \
19 : last_ack_sent  \n \
20 : last_data_recv \n \
21 : last_ack_recv  \n \
\n \
Metrics\n \
22 : pmtu \n \
23 : rcv_ssthresh \n \
24 : rtt  \n \
25 : rttvar   \n \
26 : snd_ssthresh \n \
27 : snd_cwnd \n \
28 : advmss   \n \
29 : reordering   \n \
\n \
30 : rcv_rtt  \n \
31 : rcv_space\n \
\n \
32 : total_retrans \n\n");
    exit(0);
}
