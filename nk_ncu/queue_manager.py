#!/usr/bin/env python

import socket, os, re

# Configuration
SOCK_PATH = "/tmp/clicksocket"
BUFFER = 1024
DEBUG_MODE = 0

# Check if socket exists
if (not os.path.exists(SOCK_PATH)) :
    print "Error : socket \""+SOCK_PATH+"\" doesn't exist. [hint : Is Click launched ?]"
    exit(1)

# Open connection
s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
s.connect(SOCK_PATH)

# Get version
s.send("\n")
data = s.recv(BUFFER).strip()
print "\nYou're now connected to "+data+"\nType \"help\" to show possible actions. Type \"exit\" to quit."

while (1) :

    # Wait for user action
    action = raw_input("\nAction : ")
    action = action.strip().split(" ")

    if (action[0] == "?") : # show current queue
        action_str = "READ manQueue.packets_list"

    elif (action[0] == "s") : # send packet
        if (action[1].isdigit()) :
            action_str = "WRITE manQueue.send_packet "+action[1]
        else :
            print "Invalid action parameter (type \"help\" to show valid actions)."
            continue # if invalid action

    elif (action[0] == "d") : # drop packet
        if (action[1].isdigit()) :
            action_str = "WRITE manQueue.drop_packet "+action[1]
        else :
            print "Invalid action parameter (type \"help\" to show valid actions)."
            continue # if invalid action

    elif (action[0] == "help" or action[0] == "h") : # show actions
        print "Actions :"
        print "- show current queue\t?"
        print "- send packet\t\ts <packet_id>"
        print "- drop packet\t\td <packet_id>"
        continue

    elif (action[0] == "exit") :
        break;

    else :
        print "Invalid action (type \"help\" to show valid actions)."
        continue # if invalid action

    # Send action
    s.send(action_str+"\n")

    # Receive answer
    data = s.recv(BUFFER).strip()
    if (data) :
        if (data.startswith("2")) :
            if (not DEBUG_MODE) :
                if (re.match("2.. Read*", data)) : # from read handler (containing data)
                    data = re.sub(".*\n.*\n", "", data, count=1) # remove two first lines
                else : # from write handler (just an acknowledgement)
                    data = "> OK"
            print data
        else :
            if (not DEBUG_MODE) : data = re.sub(".*\n5.. ", "", data, count=1) # remove first line and error code
            print "Error : "+data
    else :
        print "Error : server closed connection."
        break

# Close socket
s.close()

