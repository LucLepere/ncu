#include <pthread.h>
#include "tcpcomm.h"

FILE *file;
char *vars_nums = NULL;
char str[STR_LENGTH];
int manual_mode = 0, buffer = BUFFER;
int sock, *new_socket;

// Thread function
void *connection_handler(void *);

int main(int argc , char *argv[])
{
    // Declarations
    struct sockaddr_in6 server, client;
    char *filename = NULL;
    char filepath_nk[FILEPATH_LENGTH];
    char filepath_host[FILEPATH_LENGTH];
    int client_socket, ca;
    int c, plotting = 0, daemon_mode = 0, port = PORT;

    // Signal handling
	struct sigaction action;
	action.sa_handler = signal_handler;
	sigemptyset(&action.sa_mask);
	action.sa_flags = SA_RESTART;
    sigaction(SIGINT, &action, NULL);

    // Parse options
    opterr = 0;
    while ((c = getopt (argc, argv, "nhb:p:dmv:f:")) != -1)
        switch (c)
        {
        case 'n':
            show_vars();
            break;
        case 'h':
            usage();
            break;
        case 'b':
            buffer = strtol(optarg, NULL, 10);
            if (buffer < MIN_BUFFER) {
                buffer = MIN_BUFFER;
            }
            break;
        case 'p':
            port = strtol(optarg, NULL, 10);
            if (port < 1024) {
                fprintf(stderr, "Cannot bind to port %u (<1024 are privileged)\n", port);
                exit(1);
            }
            if (port > 65535) {
                fprintf(stderr, "Port %d doesn't exist (>65535)\n", port);
                exit(1);
            }
            break;
        case 'd':
            daemon_mode = 1;
            break;
        case 'm':
            manual_mode = 1;
            break;
        case 'v':
            vars_nums = optarg;
            break;
        case 'f':
            filename = optarg;
            break;
        case '?':
            if (optopt == 'v' || optopt == 'f' || optopt == 'p' || optopt == 'b')
                fprintf(stderr, "Option -%c requires an argument.\n", optopt);
            else if (isprint(optopt))
                fprintf(stderr, "Unknown option `-%c'.\n", optopt);
            else
                fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
            return 1;
        default:
            abort();
        }

    // Process related to output file
    if (filename != NULL)
    {
        // Ensure at least one plotted variable
        if (vars_nums == NULL)
        {
            fprintf(stderr, "At least one variable must be indicated with option -v to produce a file\n");
            return 1;
        }

        // Open file
        sprintf(filepath_nk, "%s/%s", PLOTDATA_FOLDER_NK, filename);
        sprintf(filepath_host, "%s/%s", PLOTDATA_FOLDER_HOST, filename);
        file = fopen(filepath_nk, "a");
        if (file == NULL)
        {
            perror("File opening failed");
            return 1;
        }

        prepare_lgdfs(filepath_host);

        // Write header for variables names
        str[0] = '\0';
        format_str_header(str, vars_nums, 1);
        fprintf(file, "%s\n", str);
        fflush(file);

        plotting = 1;
    }
    else // If no file specified
    {
        file = stdout;
    }

    if (daemon_mode)
    {
        if (plotting)
        {
            fprintf(stderr, "Cannot run in daemon mode while producing a file\n");
            return 1;
        }
        if (manual_mode)
        {
            fprintf(stderr, "Cannot run in daemon mode along with manual mode\n");
            return 1;
        }

        pid_t process_id = 0;
        pid_t sid = 0;
        process_id = fork();
        if (process_id < 0)
        {
            perror("Fork failed");
            exit(1);
        }
        if (process_id > 0)
        {
            exit(0);
        }

        umask(0);
        sid = setsid();
        if(sid < 0)
        {
            perror("Setsid failed");
            exit(1);
        }

        if ((chdir("/")) < 0)
        {
            perror("Chdir failed");
            exit(1);
        }
        fclose(stdin);
        fclose(stdout);
        fclose(stderr);
    }

    // Create socket
    sock = socket(AF_INET6, SOCK_STREAM, 6);
    if (sock == -1)
    {
        perror("Socket creation failed");
        return 1;
    }
    printf("Socket created\n");

    server.sin6_family = AF_INET6;
    server.sin6_addr = in6addr_any;
    server.sin6_port = htons(port);

    // Bind
    if (bind(sock,(struct sockaddr *)&server, sizeof(server)) < 0)
    {
        perror("Bind failed");
        return 1;
    }
    printf("Bind done\n");

    // Listen
    if (plotting) listen(sock, 1);
    else listen(sock, 3);

    // Accept incoming connections
    printf("Waiting for incoming connections...\n");
    ca = sizeof(struct sockaddr_in6);

    while ((client_socket = accept(sock, (struct sockaddr *)&client, (socklen_t*)&ca)))
    {
        printf("Connection accepted\n");

        pthread_t thread;
        new_socket = malloc(1);
        *new_socket = client_socket;

        // Don't run a thread if plotting variables
        if (plotting)
        {
            connection_handler((void*) new_socket);
        }
        else
        {
            if (pthread_create(&thread, NULL, connection_handler, (void*) new_socket) < 0)
            {
                perror("Thread creation failed");
                return 1;
            }
            if (pthread_detach(thread) != 0)
            {
                perror("Detaching thread failed");
                return 1;
            }
        }
    }

    if (client_socket < 0)
    {
        perror("Accept failed");
        return 1;
    }

    return 0;
}

// This will handle connection for each client
void *connection_handler(void *socket)
{
    struct tcp_info tcp_info;
    int tcp_info_length;

    // Get socket descriptor
    int sock = *(int*)socket;
    int read_size;
    char received_data[buffer];

    // Get time
    struct timeval tv;
    if (gettimeofday(&tv, NULL) != 0)
    {
        fprintf(stderr, "Cannot get time (gettimeofday failed)\n");
    }

    // Receive data from client
    pthread_t thread_id = pthread_self();
    int i = 1;
    while((read_size = recv(sock, received_data, buffer, 0)) > 0)
    {
        // Fill tcp_info structure
        tcp_info_length = sizeof(tcp_info);
        if (getsockopt(sock, SOL_TCP, TCP_INFO, (void *)&tcp_info, (socklen_t *)&tcp_info_length ) == 0)
        {
            str[0] = '\0';
            format_str_vars(str, &tcp_info, vars_nums, 1, 1, &tv);
            fprintf(file, "%s", str);
            fflush(file);
            if (manual_mode) // manual mode
            {
                printf("Press enter to continue receiving...\n");
                getchar();
            }
        }

        if (DEBUG_MODE)
        {
            fprintf(stdout, "[%lu] received %d\n", (unsigned long)thread_id, i);
            fflush(stdout);
            i++;
        }
    }

    if(read_size == 0)
    {
        printf("Client disconnected\n");
        fflush(stdout);
    }
    else if(read_size == -1)
    {
        perror("Receive failed");
    }

    free(socket);
    return 0;
}

void signal_handler(int signal)
{
	if(signal==SIGINT)
	{
	    fprintf(stderr, "\nClosing...\n");
        close(sock);
        fclose(file);
	}
	exit(0);
}

void usage()
{
    printf("Usage: ./receiver\n\
       [-d]\t\t daemon mode\n\
       [-v variables]\t e.g. : 7+15+26 (see -n option) (1)\n\
       [-f filename]\n\
       [-p port]\n\
       [-b buffersize]\t (in bytes)\n\
       [-m]\t\t manual mode (you will be asked to continue receiving)\n\
       [-n]\t\t show tcp_info variables names and numbers\n\
       [-h]\t\t show help\n\
       \n\
(1) Numbers correspond to tcp_info variables. They have to be separated by '+'. You can choose as many as you want.\n\n");
    exit(0);
}
