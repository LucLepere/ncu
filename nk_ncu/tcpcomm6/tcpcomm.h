#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <fnmatch.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>

// Configuration

#define LGDFS_FILE              "/hosthome/LiveGraph/tcpcomm.lgdfs"
#define PLOTDATA_FOLDER_NK      "/hosthome/Documents/tcpcomm_data"
#define PLOTDATA_FOLDER_HOST    "/home/__USERNAME__/Documents/tcpcomm_data"

#define DEBUG_MODE              0
#define PORT                    8888
#define BUFFER                  4096
#define MIN_BUFFER              576
#define STR_LENGTH              1024
#define FILEPATH_LENGTH         256

void    usage();
void    show_vars();
void    signal_handler(int signal);

int     is_valid_ip_address(char *ip_address);
double  time_to_seconds(struct timeval *tstart, struct timeval *tfinish);

void    prepare_lgdfs(char *filepath);
void    format_str_header(char *str, char *vars_nums, int nb_pos);
void    format_str_vars(char *str, struct tcp_info *tcp_info, char *vars_nums, int pos, int nb_pos, struct timeval *tstart);
int     get_tcpi_var(struct tcp_info *tcp_info, int n);
char*   get_tcpi_var_name(int n);
