#include "tcpcomm.h"

FILE *file;
int sock;

int main(int argc , char *argv[])
{
    // Declarations
    struct sockaddr_in6 server;
    struct tcp_info tcp_info;
    char *vars_nums = NULL, *filename = NULL, *ip_address = NULL;
    char filepath_nk[FILEPATH_LENGTH];
    char filepath_host[FILEPATH_LENGTH];
    char str[STR_LENGTH];
    int tcp_info_length;
    int manual_mode = 0;
    int c, pos = 1, nb_pos = 1, port = PORT, buffer = BUFFER;

    // Signal handling
	struct sigaction action;
	action.sa_handler = signal_handler;
	sigemptyset(&action.sa_mask);
	action.sa_flags = SA_RESTART;
    sigaction(SIGINT, &action, NULL);

    // Parse options
    if (argc < 2) usage();
    opterr = 0;
    while ((c = getopt(argc, argv, "nhb:p:mv:f:P:")) != -1)
        switch (c)
        {
        case 'n':
            show_vars();
            break;
        case 'h':
            usage();
            break;
        case 'b':
            buffer = strtol(optarg, NULL, 10);
            if (buffer < MIN_BUFFER) {
                buffer = MIN_BUFFER;
            }
            break;
        case 'p':
            port = strtol(optarg, NULL, 10);
            if (port < 1024) {
                fprintf(stderr, "Cannot bind to port %d (<1024 are privileged)\n", port);
                exit(1);
            }
            if (port > 65535) {
                fprintf(stderr, "Port %d doesn't exist (>65535)\n", port);
                exit(1);
            }
            break;
        case 'm':
            manual_mode = 1;
            break;
        case 'v':
            vars_nums = optarg;
            break;
        case 'f':
            filename = optarg;
            break;
        case '?':
            if (optopt == 'v' || optopt == 'f' || optopt == 'p' || optopt == 'b')
                fprintf(stderr, "Option -%c requires an argument.\n", optopt);
            else if (isprint(optopt))
                fprintf(stderr, "Unknown option `-%c'.\n", optopt);
            else
                fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
            return 1;
        default:
            abort();
        }

    // Address argument
    if (optind >= argc) {
        fprintf(stderr, "IP address argument expected after options\n");
        exit(1);
    }
    ip_address = argv[optind];
    if (!is_valid_ip_address(ip_address))
    {
        fprintf(stderr, "Invalid IP address\n");
        exit(1);
    }

    // Process related to output file
    if (filename != NULL)
    {
        // Ensure at least one plotted variable
        if (vars_nums == NULL)
        {
            fprintf(stderr, "At least one variable must be indicated with option -v to produce a file\n");
            return 1;
        }

        // Open file
        sprintf(filepath_nk, "%s/%s", PLOTDATA_FOLDER_NK, filename);
        sprintf(filepath_host, "%s/%s", PLOTDATA_FOLDER_HOST, filename);
        file = fopen(filepath_nk, "a");
        if (file == NULL)
        {
            perror("File opening failed");
            return 1;
        }

        prepare_lgdfs(filepath_host);

        // Write header for variables names
		str[0] = '\0';
		format_str_header(str, vars_nums, nb_pos);
		fprintf(file, "%s\n", str);
		fflush(file);
    }
    else // If no file specified
    {
        file = stdout;
    }

    // Create socket
    sock = socket(AF_INET6, SOCK_STREAM, 6);
    if (sock == -1)
    {
        perror("Socket creation failed");
        return 1;
    }
    printf("Socket created\n");

    inet_pton(AF_INET6, ip_address, &server.sin6_addr);
    server.sin6_family = AF_INET6;
    server.sin6_port = htons(port);

    // Connect to remote server
    if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        perror("Connection failed");
        signal_handler(SIGINT);
        return 1;
    }
    printf("Connected\n");

    // Get random data
    FILE *random_data_file = fopen("/dev/urandom", "r");
    char random_data[buffer];
    if (fread(&random_data, sizeof(random_data), 1, random_data_file) < 1)
    {
        perror("Read failed");
        return 1;
    }
    fclose(random_data_file);

    // Get time
    struct timeval tv;
    gettimeofday(&tv, NULL);

    // Sending loop
    while(1)
    {
        // Fill tcp_info structure
        tcp_info_length = sizeof(tcp_info);
        if (getsockopt(sock, SOL_TCP, TCP_INFO, (void *)&tcp_info, (socklen_t *)&tcp_info_length ) == 0)
        {
            str[0] = '\0';
            format_str_vars(str, &tcp_info, vars_nums, pos, nb_pos, &tv);
            fprintf(file, "%s", str);
            fflush(file);
            if (manual_mode) // manual mode
            {
                printf("Press enter to continue sending...\n");
                getchar();
            }
        }

        // Send data
        if(send(sock, random_data, buffer, 0) < 0)
        {
            perror("Send failed");
            signal_handler(SIGINT);
            return 1;
        }
    }

    fclose(file);
    close(sock);
    return 0;
}

void signal_handler(int signal)
{
	if(signal==SIGINT)
	{
	    fprintf(stderr, "\nClosing...\n");
        close(sock);
        fclose(file);
	}
	exit(0);
}

void usage()
{
    printf("Usage: ./sender [options] <ip_address>\n\
       [-v variables]\t e.g. : 7+15+26 (see -n option) (1)\n\
       [-f filename]\n\
       [-p port]\n\
       [-b buffersize]\t (in bytes)\n\
       [-m]\t\t manual mode (you will be asked to continue sending)\n\
       [-n]\t\t show tcp_info variables names and numbers\n\
       [-h]\t\t show help\n\
       \n\
(1) Numbers correspond to tcp_info variables. They have to be separated by '+'. You can choose as many as you want.\n\
\n\n");
    exit(0);
}
