// -*- c-basic-offset: 4 -*-
/*
 * managedqueue.{cc,hh} -- managed queue element
 * Luc Lepere
 *
 * Copyright (c) 2013 Universite Catholique de Louvain
 */

#include <click/config.h>
#include "managedqueue.hh"
#include <click/args.hh>
#include <click/error.hh>

CLICK_DECLS

ManagedQueue::ManagedQueue()
    : _q(0)
{
}

ManagedQueue::~ManagedQueue()
{
}

void *
ManagedQueue::cast(const char *n)
{
    if (strcmp(n, "Storage") == 0)
        return (Storage *)this;
    else if (strcmp(n, "ManagedQueue") == 0
             || strcmp(n, "Queue") == 0)
        return (Element *)this;
    else
        return 0;
}

int
ManagedQueue::configure(Vector<String> &conf, ErrorHandler *errh)
{
    // Create and configure IPPrintCustom element (temporary)
    ipp = new IPPrintCustom();
    ipp->configure(conf, errh);

    // Configure this element (underlying simple queue)
    unsigned new_capacity = 1000;
    //unsigned new_mancapacity = 10;
    unsigned new_mancapacity = DEFAULT_MANCAPACITY;
    if (Args(conf, this, errh)
        .read_p("CAPACITY", new_capacity)
        .read_p("MAN_CAPACITY", new_mancapacity)
        .complete() < 0)
        return -1;
    _capacity = new_capacity;
    _mancapacity = new_mancapacity;
    return 0;
}

int
ManagedQueue::initialize(ErrorHandler *errh)
{
    _mantail = NULL;
    _manhead = NULL;
    _manlength = 0;
    packet_index = 1;

    // Initialize IPPrintCustom element (temporary)
    ipp->initialize(errh);

    // Initialize this element (underlying simple queue)
    assert(!_q && _head == 0 && _tail == 0);
    _q = (Packet **) CLICK_LALLOC(sizeof(Packet *) * (_capacity + 1));
    if (_q == 0)
        return errh->error("out of memory");
    _drops = 0;
    _highwater_length = 0;
    return 0;
}

int
ManagedQueue::live_reconfigure(Vector<String> &conf, ErrorHandler *errh)
{
    // change the maximum queue length at runtime
    Storage::index_type old_capacity = _capacity;
    // NB: do not call children!
    if (ManagedQueue::configure(conf, errh) < 0)
        return -1;
    if (_capacity == old_capacity || !_q)
        return 0;
    Storage::index_type new_capacity = _capacity;
    _capacity = old_capacity;

    Packet **new_q = (Packet **) CLICK_LALLOC(sizeof(Packet *) * (new_capacity + 1));
    if (new_q == 0)
        return errh->error("out of memory");

    Storage::index_type i, j;
    for (i = _head, j = 0; i != _tail && j != new_capacity; i = next_i(i))
        new_q[j++] = _q[i];
    for (; i != _tail; i = next_i(i))
        _q[i]->kill();

    CLICK_LFREE(_q, sizeof(Packet *) * (_capacity + 1));
    _q = new_q;
    _head = 0;
    _tail = j;
    _capacity = new_capacity;
    return 0;
}

void
ManagedQueue::take_state(Element *e, ErrorHandler *errh)
{
    ManagedQueue *q = (ManagedQueue *)e->cast("ManagedQueue");
    if (!q)
        return;

    if (_tail != _head || _head != 0)
    {
        errh->error("already have packets enqueued, can%,t take state");
        return;
    }

    _head = 0;
    Storage::index_type i = 0, j = q->_head;
    while (i < _capacity && j != q->_tail)
    {
        _q[i] = q->_q[j];
        i++;
        j = q->next_i(j);
    }
    _tail = i;
    _highwater_length = size();

    if (j != q->_tail)
        errh->warning("some packets lost (old length %d, new capacity %d)",
                      q->size(), _capacity);
    while (j != q->_tail)
    {
        q->_q[j]->kill();
        j = q->next_i(j);
    }
    q->set_head(0);
    q->set_tail(0);
}

void
ManagedQueue::cleanup(CleanupStage)
{
    for (Storage::index_type i = _head; i != _tail; i = next_i(i))
        _q[i]->kill();
    CLICK_LFREE(_q, sizeof(Packet *) * (_capacity + 1));
    _q = 0;

    PacketNode* fp = _manhead;
    while(fp != NULL)
    {
        PacketNode* tmp = fp->next;
        CLICK_LFREE(fp, sizeof(PacketNode));
        fp = tmp;
    }
}

void
ManagedQueue::send_packet(Packet *p)
{
    Storage::index_type h = _head, t = _tail, nt = next_i(t);
    if (nt != h)
    {
        _q[t] = p;
        packet_memory_barrier(_q[t], _tail);
        _tail = nt;

        int s = size(h, nt);
        if (s > _highwater_length)
            _highwater_length = s;
    }
    else
    {
        if (_drops == 0 && _capacity > 0)
            click_chatter("%{element}: overflow", this);
        _drops++;
        checked_output_push(1, p);
    }
}

void
ManagedQueue::push(int, Packet *p)
{
    // Add to the queue
    addLast(p);
}

Packet *
ManagedQueue::pull(int)
{
    // If there is a packet that must be sent (i.e. in the queue), just send it
    return deq();
}

String
ManagedQueue::read_handler(Element *e, void *thunk)
{
    ManagedQueue *q = static_cast<ManagedQueue *>(e);
    int which = reinterpret_cast<intptr_t>(thunk);
    switch (which)
    {
    case 0: // length
        return String(q->size());
    case 1: // highwater_length
        return String(q->highwater_length());
    case 2: // capacity
        return String(q->capacity());
    case 3: // drops
        return String(q->_drops);
    case 4: // packets_list
        return q->list();
    default:
        return "";
    }
}

void
ManagedQueue::reset()
{
    while (Packet *p = pull(0))
        checked_output_push(1, p);
}

int
ManagedQueue::write_handler(const String &s, Element *e, void *thunk, ErrorHandler *errh)
{
    ManagedQueue *q = static_cast<ManagedQueue *>(e);
    int which = reinterpret_cast<intptr_t>(thunk);
    switch (which)
    {
    case 0: // reset_counts
        q->_drops = 0;
        q->_highwater_length = q->size();
        return 0;
    case 1: // reset
        q->reset();
        return 0;
    case 2: // send_packet
    {
        // Get param packet id to be sent
        int i;
        if (!IntArg().parse(s, i))
            return errh->error("syntax error");

        // Get and send selected packet
        Packet* p = q->removeById(i);
        if (p == NULL) return errh->error("bad packet id (unknown)");
        q->send_packet(p);
        return 0;
    }
    case 3: // drop_packet
    {
        // Get param packet id to be dropped
        int i;
        if (!IntArg().parse(s, i))
            return errh->error("syntax error");

        // Drop selected packet
        Packet* p = q->removeById(i);
        if (p == NULL) return errh->error("bad packet id (unknown)");
        p->kill();
        return 0;
    }
    default:
        return errh->error("internal error");
    }
}

void
ManagedQueue::add_handlers()
{
    add_read_handler("length", read_handler, 0);
    add_read_handler("highwater_length", read_handler, 1);
    add_read_handler("capacity", read_handler, 2, Handler::CALM);
    add_read_handler("drops", read_handler, 3);
    add_read_handler("packets_list", read_handler, 4);
    add_write_handler("capacity", reconfigure_keyword_handler, "0 CAPACITY");
    add_write_handler("reset_counts", write_handler, 0, Handler::BUTTON | Handler::NONEXCLUSIVE);
    add_write_handler("reset", write_handler, 1, Handler::BUTTON);
    add_write_handler("send_packet", write_handler, 2);
    add_write_handler("drop_packet", write_handler, 3);
}

CLICK_ENDDECLS
ELEMENT_PROVIDES(Storage)
EXPORT_ELEMENT(ManagedQueue)
ELEMENT_REQUIRES(userlevel)
