// -*- c-basic-offset: 4 -*-
#ifndef CLICK_MANAGEDQUEUE_HH
#define CLICK_MANAGEDQUEUE_HH
#include <click/element.hh>
#include <click/standard/storage.hh>
#include "elements/local/ipprintcustom.hh" // temporary

#define DEFAULT_MANCAPACITY 10

CLICK_DECLS

/*
=c

ManagedQueue
ManagedQueue(CAPACITY)

=s storage

stores packets in a FIFO queue

=d

Stores incoming packets in order to allow the user to manage these packets
through the QueueManager (communicating via socket handlers in userlevel).
The user can choose the packet to be sent and when. When selected by user,
the packets are stored ina classical first-in-first-out queue (i.e. a simple queue).
Drops incoming packets if the managed storage holds MAN_CAPACITY packets.
Drops incoming packets if the queue already holds CAPACITY packets.
The default for MAN_CAPACITY is 10.
The default for CAPACITY is 1000.

B<Multithreaded Click note:> ManagedQueue is designed to be used in an
environment with at most one concurrent pusher and at most one concurrent
puller.  Thus, at most one thread pushes to the ManagedQueue at a time and at
most one thread pulls from the ManagedQueue at a time.  Different threads can
push to and pull from the ManagedQueue concurrently, however.  See
ThreadSafeQueue for a queue that can support multiple concurrent pushers and
pullers.

=n

The Queue and NotifierQueue elements act like ManagedQueue, but additionally
notify interested parties when they change state (from nonempty to empty or
vice versa, and/or from nonfull to full or vice versa) and without any
queue management by the user.

=h length read-only

Returns the current number of packets in the queue.

=h highwater_length read-only

Returns the maximum number of packets that have ever been in the queue at once.

=h capacity read/write

Returns or sets the queue's capacity.

=h drops read-only

Returns the number of packets dropped by the queue so far.  Dropped packets
are emitted on output 1 if output 1 exists.

=h reset_counts write-only

When written, resets the C<drops> and C<highwater_length> counters.

=h reset write-only

When written, drops all packets in the queue.

=h packets_list read-only

Returns a string showing the packets (id and info) contained in the managed queue.

=h send_packet write-only

Sends (i.e. place in underlying simple queue) the packet associated
with the id passed as argument.

=a SimpleQueue, Queue, NotifierQueue, MixedQueue, RED, FrontDropQueue, ThreadSafeQueue */


typedef struct PacketNode PacketNode;

struct PacketNode
{
    int packet_id;
    Packet* packet;
    String* packet_info;
    PacketNode* next;
};


class ManagedQueue : public Element, public Storage
{
public:

    ManagedQueue();
    ~ManagedQueue();

    int drops() const               { return _drops; }
    int highwater_length() const    { return _highwater_length; }

    inline bool enq(Packet*);
    inline void lifo_enq(Packet*);
    inline Packet* deq();

    // to be used with care
    Packet* packet(int i) const     { return _q[i]; }
    void reset();				    // NB: does not do notification

    template <typename Filter> Packet* yank1(Filter);
    template <typename Filter> Packet* yank1_peek(Filter);
    template <typename Filter> int yank(Filter, Vector<Packet *> &);

    const char *class_name() const  { return "ManagedQueue"; }
    const char *port_count() const  { return PORTS_1_1X2; }
    const char *processing() const  { return "h/lh"; }
    void* cast(const char*);

    int configure(Vector<String>&, ErrorHandler*);
    int initialize(ErrorHandler*);
    void cleanup(CleanupStage);
    bool can_live_reconfigure() const { return true; }
    int live_reconfigure(Vector<String>&, ErrorHandler*);
    void take_state(Element*, ErrorHandler*);
    void add_handlers();

    void push(int port, Packet*);
    Packet* pull(int port);

    void send_packet(Packet*);

    IPPrintCustom* ipp; // temporary : IPPrintCustom element used for packet info printing

    // Managed queue variables
    int _manlength;
    PacketNode* _manhead;
    PacketNode* _mantail;
    int packet_index;

    // Managed queue functions
    inline String list();
    inline void addLast(Packet*);
    inline Packet* removeById(int);

protected:

    Packet* volatile * _q;
    volatile int _drops;
    int _highwater_length;
    index_type _mancapacity;

    friend class MixedQueue;
    friend class TokenQueue;
    friend class InOrderQueue;
    friend class ECNQueue;

    static String read_handler(Element*, void*);
    static int write_handler(const String&, Element*, void*, ErrorHandler*);

};

// Create a string containing the list of packets with their id and info
inline String
ManagedQueue::list()
{
    String s = "";
    PacketNode* n = _manhead;
    while(n != NULL)
    {
        s += (new String(n->packet_id))->c_str();
        s += "\t";
        s += n->packet_info->c_str();
        s += "\n";
        n = n->next;
    }
    for (int i=0; i<(_mancapacity - _manlength); i++)
    {
        s += "(empty)\n"; // show empty slots
    }
    return s;
}

// Add packet in the managed queue with id and info
inline void
ManagedQueue::addLast(Packet* p)
{
    if (_manlength < _mancapacity) // Check if slot available
    {
        // Call IPPrintCustom for that packet (temporary)
        string line = ipp->get_info(p);
        String *str = new String(line.c_str());

        // Prepare PacketNode struct
        PacketNode* n = (PacketNode*) CLICK_LALLOC(sizeof(PacketNode));
        //PacketNode* n = (PacketNode*) malloc(sizeof(PacketNode));

        // Fill PacketNode struct
        n->packet_id = packet_index;
        n->packet = p;
        n->packet_info = str;
        packet_index++;

        // Add packet node the managed queue
        _manlength++;
        n->next = NULL;
        if(_mantail == NULL)
        {
            _manhead = n;
            _mantail = n;
        }
        else
        {
            _mantail->next = n;
            _mantail = n;
        }
    }
    else // No more slot available, drop packet
    {
        p->kill();
    }
}

// Remove packet node corresponding to provided id from the managed queue if it exists and return packet
inline Packet*
ManagedQueue::removeById(int id)
{
    PacketNode* prec_n = NULL;
    PacketNode* n = _manhead;
    while(n != NULL)
    {
        if(n->packet_id == id)
        {
            if ((n == _manhead) || (n == _mantail))
            {
                if (n == _manhead)
                {
                    _manhead = n->next;
                }
                if (n == _mantail)
                {
                    _mantail = prec_n;
                    if (prec_n != NULL) prec_n->next = NULL;
                }
            }
            else
            {
                prec_n->next = n->next;
            }
            Packet* p = n->packet;
            //CLICK_LFREE(n, sizeof(PacketNode)); // seems to kill (free) Packet, what is not desirable here
            free(n);                              // so, just free packet node
            _manlength--;
            return p;
        }
        prec_n = n;
        n = n->next;
    }
    return NULL;
}

inline bool
ManagedQueue::enq(Packet *p)
{
    assert(p);
    Storage::index_type h = _head, t = _tail, nt = next_i(t);
    if (nt != h)
    {
        _q[t] = p;
        packet_memory_barrier(_q[t], _tail);
        _tail = nt;
        int s = size(h, nt);
        if (s > _highwater_length)
            _highwater_length = s;
        return true;
    }
    else
    {
        p->kill();
        _drops++;
        return false;
    }
}

inline void
ManagedQueue::lifo_enq(Packet *p)
{
    // XXX NB: significantly more dangerous in a multithreaded environment
    // than plain (FIFO) enq().
    assert(p);
    Storage::index_type h = _head, t = _tail, ph = prev_i(h);
    if (ph == t)
    {
        t = prev_i(t);
        _q[t]->kill();
        _tail = t;
    }
    _q[ph] = p;
    packet_memory_barrier(_q[ph], _head);
    _head = ph;
}

inline Packet *
ManagedQueue::deq()
{
    Storage::index_type h = _head, t = _tail;
    if (h != t)
    {
        Packet *p = _q[h];
        packet_memory_barrier(_q[h], _head);
        _head = next_i(h);
        assert(p);
        return p;
    }
    else
        return 0;
}

template <typename Filter>
Packet *
ManagedQueue::yank1(Filter filter)
/* Remove from the queue and return the first packet that matches
   'filter(Packet *)'. The returned packet must be deallocated by the
   caller. */
{
    for (Storage::index_type trav = _head; trav != _tail; trav = next_i(trav))
        if (filter(_q[trav]))
        {
            Packet *p = _q[trav];
            int prev = prev_i(trav);
            while (trav != _head)
            {
                _q[trav] = _q[prev];
                trav = prev;
                prev = prev_i(prev);
            }
            _head = next_i(_head);
            return p;
        }
    return 0;
}

template <typename Filter>
Packet *
ManagedQueue::yank1_peek(Filter filter)
/* return the first packet that matches
   'filter(Packet *)'. The returned packet must *NOT* be deallocated by the
   caller. */
{
    for (Storage::index_type trav = _head; trav != _tail; trav = next_i(trav))
        if (filter(_q[trav]))
        {
            Packet *p = _q[trav];
            return p;
        }
    return 0;
}

template <typename Filter>
int
ManagedQueue::yank(Filter filter, Vector<Packet *> &yank_vec)
/* Removes from the queue and adds to 'yank_vec' all packets in the queue
   that match 'filter(Packet *)'. Packets are added to 'yank_vec' in LIFO
   order, so 'yank_vec.back()' will equal the first packet in the queue
   that matched 'filter()'. Caller should deallocate any packets returned
   in 'yank_vec'. Returns the number of packets yanked. */
{
    Storage::index_type write_ptr = _tail;
    int nyanked = 0;
    for (Storage::index_type trav = _tail; trav != _head; )
    {
        trav = prev_i(trav);
        if (filter(_q[trav]))
        {
            yank_vec.push_back(_q[trav]);
            nyanked++;
        }
        else
        {
            write_ptr = prev_i(write_ptr);
            _q[write_ptr] = _q[trav];
        }
    }
    _head = write_ptr;
    return nyanked;
}

CLICK_ENDDECLS
#endif
