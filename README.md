# Networking courses utils (NCU) #

Before running anything, you must ensure you have the following applications correctly installed :

* [Netkit](http://netkit.org)

* [Wireshark](http://wireshark.org)


## Install LiveGraph ##

[LiveGraph](http://www.live-graph.org) can be downloaded [here](http://downloads.sourceforge.net/live-graph/LiveGraph.2.0.beta01.Complete.zip).
Just extract it somewhere (example : `/home/foo/LiveGraph`).


## Set up environment variables ##

Edit your `~/.bash_profile` (or `~/.bashrc`) file and add the following lines (replacing "foo" with your username).

```
#!bash
# Netkit-fellow
export NKF_HOME=/home/foo/nk-fellow
export NKF_LG_DATA=/home/foo/Documents/tcpcomm_data
export NKF_WS_DATA=/home/foo/Documents/wireshark_data
export LG_DIR=/home/foo/LiveGraph

```
Please note that `$LG_DIR` is the directory containing the `LiveGraph.jar` file.

Don't forget to check that `$NKF_LG_DATA` and `$NKF_WS_DATA` directories exist. Create it if needed.

Pay attention to the "nk-fellow" directory where you'll need to install it accordingly.


## Install Netkit fellow ##

Copy `res/nk-fellow` somewhere (in your home directory by example).

Then, run it.

```
#!bash
$ $NKF_HOME/nk-fellow
```
Maybe you will have to install some missing packages.

NB : This daemon should always be run before using NCU.


## Using custom Netkit filesystem ##

Please use the provided Netkit filesystem available in `res/netkit-filesystem-custom.tar.bz2`.
For this, extract the archive and place it in your Netkit installation directory.
```
#!bash
$ tar -xjSf netkit-filesystem-custom.tar.bz2
$ mv fs $NETKIT_HOME
```

This filesystem has the following changes installed : `gcc`, `make`, `g++`, `iperf`, `IO-Interface` Perl extension and the Python package `netaddr`.


## Patch and install Autonetkit ##

Apply the patch provided in the `res` directory on the application Autonetkit version 0.9.6 which can be downloaded [here](https://github.com/sk2/autonetkit).
```
#!bash
$ cd foo/autonetkit-0.9.6
$ patch -p1 -i autonetkit.patch
```

Then, you can install Autonetkit.
```
#!bash
$ cd foo/autonetkit-0.9.6
$ python setup.py install
```


## Prepare NCU tools for Netkit ##

Just copy the `nk_ncu` directory into your home directory.

Now enter the `nk_ncu` directory and run the `sed` command following (replacing "foo" with your username). Do no touch "\_\_USERNAME\_\_" ! Replace only "foo".
```
#!bash
$ cd foo/nk_ncu
$ grep -rl . | xargs sed -i s@__USERNAME__@foo@g
```


## Install exagen ##

Follow the instructions provided in the exagen README file.