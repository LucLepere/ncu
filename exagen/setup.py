import os
from setuptools import setup, find_packages

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "exagen",
    version = "0.1",
    author = "Luc Lepere",
    author_email = "luc@lepere.be",
    description = ("Networking courses exams generator"),
    license = "GPL",
    keywords = "netkit networks courses",
    packages=find_packages(),
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
    entry_points = {
        'console_scripts': [
            'create_topo = exagen.create_topo:main',
            'config_ip = exagen.config_ip:main',
            'create_lab = exagen.create_lab:main',
            'add_errors = exagen.add_errors:main',
        ],
    },
    package_data={'': ['templates/*', 'config/*', '*.txt']},
    install_requires = [
         'netaddr>=0.7.10',
         'networkx>=1.7',
         'py2-ipaddress',
         ],
)

