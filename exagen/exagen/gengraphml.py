#!/usr/bin/env python

from utils import *
from string import Template

# Templates
TEMPLATES_DIR       = "templates/"
NODE_TEMPLATE       = TEMPLATES_DIR+"node.xml"
LINK_TEMPLATE       = TEMPLATES_DIR+"link.xml"
GRAPHML_TEMPLATE    = TEMPLATES_DIR+"empty.graphml"


def createNode(graph, n, template) :
    """
        Create node in XML for GRAPHML file format for the given node.

        :param template: XML template of a node
        :type template: opened file object
        :return: node formatted in XML
        :rtype: string
    """
    # Create template
    n_str = Template(template)
    
    # Get values
    ident           = n
    label           = getNodeVar(graph, n, LABEL)
    dev_type        = getNodeVar(graph, n, DEV_TYPE)
    netkit_memory   = getNodeVar(graph, n, NK_MEM)
    x               = getNodeVar(graph, n, POS_X)
    y               = getNodeVar(graph, n, POS_Y)

    if (dev_type == ROUTER) :
        size = 'height="31.250017166137695" width="46.25"'
    else :
        size = 'height="37.48875045776367" width="26.231876373291016"'

    # Substitute in template
    n_str = n_str.substitute(ident=ident, netkit_memory=netkit_memory, size=size, x=x, y=y, label=label, dev_type=dev_type)
    return n_str


def createLink(graph, e, template) :
    """
        Create edge in XML for GRAPHML file format for the given edge.

        :param template: XML template of an edge
        :type template: opened file object
        :return: edge formatted in XML
        :rtype: string
    """
    n_src, n_tgt = e

    # Create template
    n_str = Template(template)
    
    # Get values
    ident                   = getEdgeVar(graph, e, ID)
    label                   = getEdgeVar(graph, e, LABEL)
    bandwidth               = getEdgeVar(graph, e, BANDWIDTH)    
    ipv4_subnet             = getEdgeVar(graph, e, SUBNET_IPV4)
    ipv6_subnet             = getEdgeVar(graph, e, SUBNET_IPV6)
    if_src_ipv4             = getIfVar(graph, n_src, e, IPV4)
    if_src_ipv6             = getIfVar(graph, n_src, e, IPV6)
    if_tgt_ipv4             = getIfVar(graph, n_tgt, e, IPV4)
    if_tgt_ipv6             = getIfVar(graph, n_tgt, e, IPV6)
    
    # Substitute in template
    n_str = n_str.substitute(ident=ident, source_id=n_src, target_id=n_tgt, bandwidth=bandwidth, label=label, interface_black_ipv4=if_src_ipv4, interface_white_ipv4=if_tgt_ipv4, interface_black_ipv6=if_src_ipv6, interface_white_ipv6=if_tgt_ipv6, ipv4_subnet=ipv4_subnet, ipv6_subnet=ipv6_subnet, text=label)
    return n_str


def generate_graphml(graph) :
    """
        Generate the GRAPHML corresponding to the given graph.

        :return: graph formatted in GRAPHML
        :rtype: string
    """

    script_dir = os.path.dirname(os.path.realpath(__file__))

    # Get graphml template
    fileObj = open(script_dir+"/"+GRAPHML_TEMPLATE, "r")
    graphml_template = fileObj.read()

    graphml_str = Template(graphml_template)

    # Get node template
    fileObj = open(script_dir+"/"+NODE_TEMPLATE, "r")
    node_template = fileObj.read()

    # Get link template
    fileObj = open(script_dir+"/"+LINK_TEMPLATE, "r")
    link_template = fileObj.read()

    # Add routers and hosts in graphml template
    for n in graph.nodes() :
        node_str = createNode(graph, n, node_template)
        node_str += "\n$nodes"
        graphml_str = Template(graphml_str.substitute(ipv6_prefix="$ipv6_prefix", nodes=node_str, links="$links"))

    graphml_str = Template(graphml_str.substitute(ipv6_prefix="$ipv6_prefix", nodes="", links="$links"))

    # Add edges in graphml template
    for e in graph.edges() :
        edge_str = createLink(graph, e, link_template)
        edge_str += "\n$links"
        graphml_str = Template(graphml_str.substitute(ipv6_prefix="$ipv6_prefix", links=edge_str))

    ipv6_prefix = ""
    graphml_str = graphml_str.substitute(ipv6_prefix=ipv6_prefix, links="")
    return graphml_str


