#!/usr/bin/env python

import networkx as nx
import ipaddress as ia
from xml.dom import minidom

from utils import *
import gengraphml


# =======================================================================
# ============================== FUNCTIONS ==============================
# =======================================================================


def check_subnet_consistency(nw_list, subnet, if_src, if_tgt) :
    """
        Check whether the link and connected interfaces are consistent in the network or not.

        :param nw_list: list of networks already existing in the network graph
        :type nw_list: list of ipaddress.IPv4Network and ipaddress.IPv6Network
        :param subnet: subnet of the link
        :type subnet: ipaddress.IPv4Network or ipaddress.IPv6Network
        :param if_src: IP address of source interface
        :type if_src: ipaddress.IPv4Address or ipaddress.IPv6Address
        :param if_tgt: IP address of target interface
        :type if_tgt: ipaddress.IPv4Address or ipaddress.IPv6Address
    """
    if (subnet) :
        if (if_src and if_src not in subnet) :
            error("IP address "+str(if_src)+" doesn't match "+str(subnet)+" subnet")
        if (if_tgt and if_tgt not in subnet) :
            error("IP address "+str(if_tgt)+" doesn't match "+str(subnet)+" subnet")
        if (if_src and if_tgt and if_src == if_tgt) :
            error("Double attribution of IP address ("+if_src+") on the same link")
        if (subnet in nw_list) :
            error("Topology two identical networks ("+subnet+")")
        nw_list.append(subnet)
    return nw_list


def check_config(graph) :
    """Check IP configuration consistency."""

    networksV4 = []
    networksV6 = []

    for e in graph.edges() :
        n_src, n_tgt = e

        # IPV4
        subnet = getEdgeVar(graph, e, SUBNET_IPV4)
        if_src = getIfVar(graph, n_src, e, IPV4)
        if_tgt = getIfVar(graph, n_tgt, e, IPV4)
        networksV4 = check_subnet_consistency(networksV4, subnet, if_src, if_tgt)
        
        # IPV6
        subnet = getEdgeVar(graph, e, SUBNET_IPV6)
        if_src = getIfVar(graph, n_src, e, IPV6)
        if_tgt = getIfVar(graph, n_tgt, e, IPV6)
        networksV6 = check_subnet_consistency(networksV6, subnet, if_src, if_tgt)


def checkIpNetwork(graph, e, var) :
    """Check IP network validity and convert it to ipaddress.IPv4Network or ipaddress.IPv6Network."""
    checkIP(graph, e, var, True)


def checkIpAddress(graph, e, var) :
    """Check IP address validity and convert it to ipaddress.IPv4Address or ipaddress.IPv6Address."""
    checkIP(graph, e, var, False)


def checkIP(graph, e, var, is_network) :
    """Check IP address or network validity and converts it to ipaddress module relevant class."""
    n_src, n_tgt = e
    var_value = getEdgeVar(graph, e, var)
    if (var_value) :
        try :
            if (is_network) :
                graph.edge[n_src][n_tgt][var] = ia.ip_network(var_value)
            else :
                graph.edge[n_src][n_tgt][var] = ia.ip_address(var_value)
        except ValueError :
            error("Bad IP address in attribute '"+var+"' of link "+str(e))


def is_edges_labelled(graph) :
    """Check whether the edges of the graph are already labelled or not."""
    for e in graph.edges() :
        if (not getEdgeVar(graph, e, LABEL)) :
            return False
    return True


def load_graphml(abs_filepath) :
    """Loads the GRAPHML file, computes all needed info, converts relevant fields in the right types (ipaddress module classes) and returns the NetworkX graph."""
    graph = nx.read_graphml(abs_filepath)
    if (not is_edges_labelled(graph)) :
        graph = attribute_edge_labels(graph)
        save_file(abs_filepath, gengraphml.generate_graphml(graph))

    # Add dev_type attribute (get manually in XML device_type value and add it in the graph as an attribute)

    # Create MiniDOM XML document
    xmldoc = minidom.parse(abs_filepath)
    graphml_elem_list = xmldoc.getElementsByTagName('graphml')
    key_elem_list = graphml_elem_list[0].getElementsByTagName('key')

    # Find id of refid SVG
    node_graph_id = 0
    for e in key_elem_list :
        if (e.hasAttribute("yfiles.type")) :
            if (e.attributes['yfiles.type'].value == "nodegraphics") :
                node_graph_id = e.attributes['id'].value

    graph_elem_list = graphml_elem_list[0].getElementsByTagName('graph')
    node_elem_list = graph_elem_list[0].getElementsByTagName('node')

    # Add device_type property depending on the SVG node
    for n in node_elem_list :

        node_id = n.attributes['id'].value

        data_elem_list = n.getElementsByTagName('data')

        # Get SVG node id and check device_type existence
        for d in data_elem_list :
            if (d.attributes['key'].value == node_graph_id) :
                cont = d.getElementsByTagName('y:SVGNode')[0].getElementsByTagName('y:SVGModel')[0].getElementsByTagName('y:SVGContent')[0]
                dev_type = cont.attributes['refid'].value
                
                graph.node[node_id][DEV_TYPE] = dev_type

    nodes_ifnos = {}
    for n in graph.nodes() :
        nodes_ifnos[n] = 0
        graph.node[n]['interfaces'] = {}

    for e in graph.edges() :

        # Convert strings in IpAddress and IpNetwork objects
        checkIpNetwork(graph, e, SUBNET_IPV4)
        checkIpNetwork(graph, e, SUBNET_IPV6)
        checkIpAddress(graph, e, IF_SRC_IPV4)
        checkIpAddress(graph, e, IF_SRC_IPV6)
        checkIpAddress(graph, e, IF_TGT_IPV4)        
        checkIpAddress(graph, e, IF_TGT_IPV6)

        # Get source interface info
        node = e[0]
        if_ipv4 = getEdgeVar(graph, e, IF_SRC_IPV4)
        if_ipv6 = getEdgeVar(graph, e, IF_SRC_IPV6)
        if_no   = nodes_ifnos[node]
        nodes_ifnos[node] += 1      
        graph = fill_if_info(graph, node, e, if_no, if_ipv4, if_ipv6)
        
        # Get target interface info
        node = e[1]
        if_ipv4 = getEdgeVar(graph, e, IF_TGT_IPV4)
        if_ipv6 = getEdgeVar(graph, e, IF_TGT_IPV6)
        if_no   = nodes_ifnos[node]
        nodes_ifnos[node] += 1      
        graph = fill_if_info(graph, node, e, if_no, if_ipv4, if_ipv6)

    check_config(graph)
    return graph


def fill_if_info(graph, n, e, if_no, if_ipv4, if_ipv6) :
    """Fills graph with the given interface info."""
    graph.node[n]['interfaces'][e]          = {}    
    graph.node[n]['interfaces'][e][IF_NO]   = if_no
    graph.node[n]['interfaces'][e][IPV4]    = if_ipv4
    graph.node[n]['interfaces'][e][IPV6]    = if_ipv6
    return graph


