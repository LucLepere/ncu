#!/usr/bin/env python

import os
import random
import ipaddress as ia
from optparse import OptionParser

import utils as ut
import gengraphml
import loadgraphml

# =======================================================================
# ============================ CONFIGURATION ============================
# =======================================================================

EXEC_YED = False
HARD_MODE = False
IPV4_NETWORKS_FILE = "config/ipv4_networks.txt"
IPV6_NETWORKS_FILE = "config/ipv6_networks.txt"


# =======================================================================
# ============================== FUNCTIONS ==============================
# =======================================================================

def gen_random_ipv6() :
    """
        Generate a random IPv6 address.

        :rtype: string
    """
    prefix = "2001:db8:be:"
    prefix += hex(random.randint(0, 2**16-1))[2:]
    prefix += "::1"
    return prefix


def get_random_subnet_v4() :
    """
        Generate a random IPv4 network.

        :rtype: ipaddress.IPv4Network
    """
    ip_random = ia.IPv4Address(random.randint(1, 0xffffffff))
    if (HARD_MODE) :        
        prefix = random.randint(2, 30)
    else :
        prefix_list = [2, 4, 8, 12, 16, 20, 24, 28, 30]
        prefix = random.choice(prefix_list)
    return ia.ip_network(str(ip_random)+"/"+str(prefix), strict=False)


def get_random_subnet_v6() :
    """
        Generate a random IPv6 network.

        :rtype: ipaddress.IPv6Network
    """
    ip_random = ia.IPv6Address(gen_random_ipv6())
    if (HARD_MODE) :        
        prefix = random.randint(2, 126)
    else :
        prefix_list = [64]
        prefix = random.choice(prefix_list)
    return ia.ip_network(str(ip_random)+"/"+str(prefix), strict=False)


def is_network_used(graph, nw) :
    """
        Check whether the NetworkX graph already contains the given network.

        :param nw: ipaddress.IPv4Network or ipaddress.IPv6Network
    """
    if (nw.version == 6) :
        attr_subnet = ut.SUBNET_IPV6
    else :
        attr_subnet = ut.SUBNET_IPV4
    for e in graph.edges() :
        if (ut.getEdgeVar(graph, e, attr_subnet) == nw) :
            return True
    return False


def complete_config_ip(graph, script_dir, ip_version) :
    """
        Complete the graph with a consistent IP configuration.

        :param ip_version: 'ipv4' or 'ipv6' (also constants ut.IPV4 and ut.IPV6)
    """
    
    # Get specific attribute names given ip version
    if (ip_version == ut.IPV6) :
        attr_subnet = ut.SUBNET_IPV6
        prios = [ia.ip_network(line.strip()) for line in open(script_dir+"/"+IPV6_NETWORKS_FILE) if line.strip()]
        bits_nb = 128
    else :
        attr_subnet = ut.SUBNET_IPV4
        prios = [ia.ip_network(line.strip()) for line in open(script_dir+"/"+IPV4_NETWORKS_FILE) if line.strip()]
        bits_nb = 32

    # Remove possible duplicates in prios networks list
    prios = list(set(prios))

    # Remove already used priority networks
    for e in graph.edges() :
        subnet = ut.getEdgeVar(graph, e, attr_subnet)
        if (subnet and subnet in prios) :
            prios.remove(subnet)

    for e in graph.edges() :
        
        n_src, n_tgt = e

        subnet = ut.getEdgeVar(graph, e, attr_subnet)
        if_src_ip = ut.getIfVar(graph, n_src, e, ip_version)
        if_tgt_ip = ut.getIfVar(graph, n_tgt, e, ip_version)

        # Subnet is defined
        if (subnet) :
            i = 1
            # Define source IP given network if not defined
            if (not if_src_ip) :
                graph.node[n_src]['interfaces'][e][ip_version] = subnet[i]
                i += 1
            # Define target IP given network if not defined
            if (not if_tgt_ip) :
                graph.node[n_tgt]['interfaces'][e][ip_version] = subnet[i]
        
        # Subnet is not defined    
        else :

            if (if_src_ip or if_tgt_ip) :

                if (if_src_ip and not if_tgt_ip) :
                    graph.node[n_tgt]['interfaces'][e][ip_version] = if_src_ip + 1
                    if_tgt_ip = if_src_ip + 1

                if (if_tgt_ip and not if_src_ip) :
                    graph.node[n_src]['interfaces'][e][ip_version] = if_tgt_ip + 1
                    if_src_ip = if_tgt_ip + 1

                # If a priority network matches, give it
                for subnet in prios :
                    if (if_src_ip in subnet and if_tgt_ip in subnet) :
                        graph.edge[n_src][n_tgt][attr_subnet] = subnet
                        prios.remove(subnet)
                        break

                if (not ut.getEdgeVar(graph, e, attr_subnet)) :
                    # Find the most little mask
                    for i in range(bits_nb-1, 0, -1) :
                        subnet = ia.ip_network(str(if_src_ip)+"/"+str(i), strict=False)
                        if (if_src_ip in subnet and if_tgt_ip in subnet) :
                            graph.edge[n_src][n_tgt][attr_subnet] = subnet
                            break

    for e in graph.edges() :
        n_src, n_tgt = e
        subnet = ut.getEdgeVar(graph, e, attr_subnet)
        if (not subnet) :
            # Try to attribute a priority
            for p in prios :
                subnet = p
                prios.remove(p)
                break
            # Total random
            if (not subnet) :
                if (ip_version == ut.IPV6) :
                    subnet = get_random_subnet_v6()
                    while (is_network_used(graph, subnet)) :
                        subnet = get_random_subnet_v6()
                else :
                    subnet = get_random_subnet_v4()
                    while (is_network_used(graph, subnet)) :
                        subnet = get_random_subnet_v4()

            graph.edge[n_src][n_tgt][attr_subnet] = subnet
            graph.node[n_src]['interfaces'][e][ip_version] = subnet[1]
            graph.node[n_tgt]['interfaces'][e][ip_version] = subnet[2]

    return graph


# ======================================================================
# ================================ MAIN ================================
# ======================================================================

def main() :

    script_dir = os.path.dirname(os.path.realpath(__file__))

    usage = "Usage: %prog [options] graphml_file"
    parser = OptionParser(usage=usage)

    # Define options
    parser.add_option("-4", "--ipv4", action="store_true", dest="ipv4", help="configure in IPv4")
    parser.add_option("-6", "--ipv6", action="store_true", dest="ipv6", help="configure in IPv6")
    parser.add_option("-d", "--dualstack", action="store_true", dest="dualstack", help="configure in dualstack (IPv4 and IPv6)")
    parser.add_option(
	    '-f',
	    '--file',
	    dest="file",
	    help='save with provided filename')
    parser.add_option(
	    '-y',
	    '--yEd',
	    action="store_true",
	    dest="open_yed",
        default = EXEC_YED,
	    help='open yEd with created network graphml')

    (options, args) = parser.parse_args()

    # Process options

    if len(args) != 1 :
        parser.error("Error : missing argument.")

    input_abs_filepath = ut.check_input(os.path.abspath(args[0]))

    if (options.file) :
        output_abs_filepath = os.path.dirname(input_abs_filepath)+"/"+options.file
    else :
        output_abs_filepath = input_abs_filepath[:-8]+"_configured.graphml"  

    # Config dualstack by default
    if (not options.ipv4 and not options.ipv6 and not options.dualstack) :
        options.dualstack = True

    graph = loadgraphml.load_graphml(input_abs_filepath)

    # Complete missing information with random

    if (options.ipv4 or options.dualstack) :
        graph = complete_config_ip(graph, script_dir, ut.IPV4)
    if (options.ipv6 or options.dualstack) :
        graph = complete_config_ip(graph, script_dir, ut.IPV6)

    output_abs_filepath = ut.get_filepath(output_abs_filepath)
    ut.save_file(output_abs_filepath, gengraphml.generate_graphml(graph))


    # Open file in yEd if needed
    if (options.open_yed) :
        ut.launch_yed(output_abs_filepath)


if __name__ == "__main__":
    main()




