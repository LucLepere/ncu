#!/usr/bin/env python

import os
import random
import matplotlib.pyplot as plt
import networkx as nx
from string import Template

import utils as ut

# =======================================================================
# ============================ CONFIGURATION ============================
# =======================================================================

LATEX_TEMPLATE = "templates/statement.tex"
EXEC_YED = False

# =======================================================================
# ============================== FUNCTIONS ==============================
# =======================================================================


def get_positions_dict(graph) :
    """
        Generate a node positions dictionary.

        :return: node positions
        :rtype: dictionary node_id -> (x, y)
    """
    pos = {}
    for n in graph.nodes() :
        x = float(ut.getNodeVar(graph, n, ut.POS_X)) / ut.YED_X_SCALE
        y = float(ut.getNodeVar(graph, n, ut.POS_Y)) / ut.YED_Y_SCALE
        pos[n] = (x, y)
    return pos


def get_edge_labels_dict(graph) :
    """
        Generate an edge labels dictionary.

        :return: edge labels
        :rtype: dictionary edge_id -> label
    """
    edge_labels = {}
    for e in graph.edges() :
        edge_labels[e] = ut.getEdgeVar(graph, e, ut.LABEL)
    return edge_labels


def get_node_labels_dict(graph) :  
    """
        Generate a node labels dictionary.

        :return: node labels
        :rtype: dictionary node_id -> label
    """ 
    node_labels = {}
    for n in graph.nodes() :
        node_labels[n] = ut.getNodeVar(graph, n, ut.LABEL)
    return node_labels


def get_routers_hosts_list(graph) :
    """
        Generate routers and hosts lists.

        :return: routers and hosts lists
        :rtype: tuple of lists (routers, hosts)
    """
    routers = []
    hosts = []
    for n in graph.nodes() :
        if (ut.getNodeVar(graph, n, ut.DEV_TYPE) == ut.ROUTER) :
            routers.append(n)
        else :
            hosts.append(n)
    return (routers, hosts)


def draw_graph_picture(graph, filename) :
    """
        Draw a PNG picture of the network.

        :param filename: absolute filepath of the picture to be created without extension
        :type filename: string
    """
    pos = get_positions_dict(graph)
    node_labels = get_node_labels_dict(graph)
    routers, hosts = get_routers_hosts_list(graph)

    nx.draw(graph, pos=pos, labels=node_labels, node_size=1500, node_shape='s', node_color='r', nodelist=routers, arrows=False, font_size=10)

    # Draw routers
    nx.draw_networkx_nodes(graph, pos=pos, labels=node_labels, node_size=1500, node_shape='s', node_color='y', nodelist=routers)

    # Draw hosts
    nx.draw_networkx_nodes(graph, pos=pos, labels=node_labels, node_size=1500, node_shape='o', node_color='w', nodelist=hosts)

    # Draw links
    nx.draw_networkx_edge_labels(graph, pos, edge_labels=get_edge_labels_dict(graph), font_size=13)

    # Save as PNG file
    plt.savefig(filename+".png", dpi=100)


def get_edges_list(graph, ip_ver) :
    """
        Returns a dictionary edge_label -> ip_subnet given the IP version.

        :param ip_ver: IP version of the subnet
        :type ip_ver:ut.SUBNET_IPV4 or ut.SUBNET_IPV6
    """
    edges = {}
    for e in graph.edges() :
        if (ip_ver == 6) :
            edges[ut.getEdgeVar(graph, e, ut.LABEL)] = ut.getEdgeVar(graph, e, ut.SUBNET_IPV6)
        else :
            edges[ut.getEdgeVar(graph, e, ut.LABEL)] = ut.getEdgeVar(graph, e, ut.SUBNET_IPV4)
    return edges


def is_there_few_shortest_paths(graph) :
    for n1 in graph.nodes() :
        for n2 in graph.nodes() :
            sh_paths = list(nx.all_shortest_paths(graph.to_undirected(), n1, n2))
            if (len(sh_paths) > 1) :
                return sh_paths
    return []


def gen_statement(graph, script_dir, abs_filepath) :
    """
        Generate a complete statement and save it in the given filepath.

        :param abs_filepath: absolute filepath of the statement
        :type abs_filepath: string
    """  

    # Create network PNG file
    draw_graph_picture(graph, abs_filepath)
    picture = os.path.basename(abs_filepath+".png")
    
    # Print links with IP
    edges_dict = get_edges_list(graph, 4)
    links_str = ""
    for k,v in edges_dict.items() :
        links_str += "\item Link "+str(k)+" has network "+str(v)+"\n"

    # Choose a prefered route if there is more than one possible shortest path
    sh_paths = is_there_few_shortest_paths(graph)

    if (sh_paths) :
        prefered_route = random.choice(sh_paths)
        pref_route_str = ""
        for n in prefered_route :
            pref_route_str += ut.getNodeVar(graph, n, ut.LABEL)+" - "
        prefered_route = pref_route_str
    else :
        prefered_route = ""

    # Save file based on template    
    fileObj = open(script_dir+"/"+LATEX_TEMPLATE, "r")
    tex_template = Template(fileObj.read())
    tex_str = tex_template.substitute(picture=picture, links=links_str, prefered_route=prefered_route)
    ut.save_file(abs_filepath+".tex", tex_str)

    # Open it
    os.system("pdflatex "+abs_filepath+".tex") 



