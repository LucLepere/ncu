#!/usr/bin/env python

import random
import os


# =======================================================================
# ============================== CONSTANTS ==============================
# =======================================================================

YED_X_SCALE = 800
YED_Y_SCALE = 600

ROUTER      = '1'
HOST        = '2'

# Common attributes
LABEL       = 'label'

# Node attributes
DEV_TYPE    = 'dev_type'
NK_MEM      = 'netkit_memory'
POS_X       = 'x'
POS_Y       = 'y'

# Edge attributes
ID          = 'id'
BANDWIDTH   = 'bandwidth'
SUBNET_IPV4 = 'ipv4_subnet'
SUBNET_IPV6 = 'ipv6_subnet'

# Interface attrbitues
IF_NO       = 'no'
IPV4        = 'ipv4'
IPV6        = 'ipv6'

IF_SRC_NO   = 'interface_white_no'
IF_SRC_IPV4 = 'interface_white_ipv4'
IF_SRC_IPV6 = 'interface_white_ipv6'

IF_TGT_NO   = 'interface_black_no'
IF_TGT_IPV4 = 'interface_black_ipv4'
IF_TGT_IPV6 = 'interface_black_ipv6'


# =======================================================================
# ============================== FUNCTIONS ==============================
# =======================================================================

def error(msg) :
    """Handle an error by printing the message and exiting program."""
    print "Error : "+str(msg)
    exit(1)


def save_file(abs_filepath, content) :
    """
        Save the given file with the given content.
        
        :param abs_filepath: absolute filepath of the file to be saved
        :type abs_filepath: string
        :param content: content to be saved in the file
        :type content: string
    """
    fileObj = open(abs_filepath, "w")
    fileObj.write(content)
    fileObj.close()


def attribute_edge_labels(graph) :
    """Attribute edge labels (L1, L2, ...)."""
    i = 1
    for n_src, n_tgt in graph.edges() :
        graph.edge[n_src][n_tgt][LABEL] = "L"+str(i)
        i += 1 
    return graph


def launch_yed(filepath) :
    """Launch yEd with the given filepath."""
    cmd = "yEd '"+filepath+"' &"
    os.system(cmd)


def launch_netkit(lab_dir) :
    """Launch Netkit with the given lab."""
    cmd = "lstart '"+lab_dir+"' &"
    os.system(cmd)


def check_input(filepath) :
    """Check filepath validity (existence and graphml format)."""
    if (not os.path.isfile(filepath)) :
        error("File doesn't exist")
    if (not filepath.endswith(".graphml")) :
        error("File doesn't seem to be a graphml file")
    return filepath


def get_nb(opt_val, var_name) :
    """
        Returns a unique option value (given value or random value between given range).

        :param opt_val: option value, exact number (e.g. 5) or range (e.g. 4-7).
        :type opt_val: string
        :param var_name: name of the option of the given number value
        :type var_name: string
    """
    opt_values = opt_val.split('-')
    if (len(opt_values) == 1) :
        return int(opt_values[0])
    if (len(opt_values) == 2) :
        if (opt_values[0] == "") :
            error("negative number")
        min_val = int(opt_values[0])
        max_val = int(opt_values[1])
        if (min_val == max_val) :
            return min_val
        if (min_val > max_val) :
            error("min number of "+var_name+" > max number of "+var_name)
        return random.randint(min_val, max_val)
    error("Bad "+var_name+" option. Use exact number (e.g. : 5) or range (e.g. : 4-7).")


def get_filepath(filepath) :
    """Returns a filepath for the given filepath but which doesn't exist."""
    if (not filepath.endswith(".graphml")) :
        filepath += ".graphml"
    if (os.path.isfile(filepath)) :
        filepath_wo_ext = filepath[:-8]
        i = 1
        while (True) :
            filepath = filepath_wo_ext+"-"+str(i)+".graphml"
            if (not os.path.isfile(filepath)) :
                return os.path.abspath(filepath)
            i += 1
    return os.path.abspath(filepath)


def getNodeVar(graph, n, var) :
    """Returns the value of the given variable of the given node."""
    if var in graph.node[n] :
        return str(graph.node[n][var]).strip()
    else :
        return ""


def getEdgeVar(graph, e, var) :
    """Returns the value of the given variable of the given edge."""
    n_src, n_tgt = e
    if var in graph.edge[n_src][n_tgt] :
        if (graph.edge[n_src][n_tgt][var] is None) :
            return ""
        if (isinstance(graph.edge[n_src][n_tgt][var], basestring)) :
            return graph.edge[n_src][n_tgt][var].strip()
        return graph.edge[n_src][n_tgt][var]
    else :
        return ""


def getIfVar(graph, n, e, var) :
    """Returns the value of the given variable of the given interface (known via node and edge)."""
    try :
        return graph.node[n]['interfaces'][e][var]
    except (ValueError, KeyError) :
        return ""


def get_edge_out(graph, n, v) :
    """Returns the edge id of the link between nodes id n and v."""
    # TODO: refactor n and v to node_id1 and nide_id2
    try :
        e = graph.edge[n][v]
        return (n,v)
    except KeyError :
        pass
    try :
        e = graph.edge[v][n]
        return (v,n)
    except KeyError :
        pass
    raise SystemError("Internal error - unexpected")


def print_graph(graph) :
    """
        Prints all variables of all the nodes and all the edges of the graph.

        .. note:: Used for debugging purpose.        
    """
    print "\n===================== NODES =====================\n"
    for n in graph.nodes() :
        var = 'n'
        print var+" : "+str(n)
        var = LABEL
        print var+" : "+getNodeVar(graph, n, var)
        var = DEV_TYPE
        print var+" : "+getNodeVar(graph, n, var)
        var = NK_MEM
        print var+" : "+getNodeVar(graph, n, var)
        var = POS_X
        print var+" : "+getNodeVar(graph, n, var)
        var = POS_Y
        print var+" : "+getNodeVar(graph, n, var)

        for i in graph.node[n]['interfaces'] :
            print "-----"
            print "(linked to edge "+str(i)+")"
            print "Interface eth"+str(getIfVar(graph, n, i, IF_NO))
            ip = getIfVar(graph, n, i, IPV4)
            print "IPV4 address : "+str(ip)+" "+str(type(ip))
            ip = getIfVar(graph, n, i, IPV6)
            print "IPV6 address : "+str(ip)+" "+str(type(ip))

        print "----------------------------------------"
    print "\n===================== EDGES =====================\n"
    for e in graph.edges() :
        var = 'e'
        print var+" : "+str(e)
        var = ID
        print var+" : "+getEdgeVar(graph, e, var)
        var = LABEL
        print var+" : "+getEdgeVar(graph, e, var)
        var = SUBNET_IPV4
        print var+" : "+str(getEdgeVar(graph, e, var))+" "+str(type(getEdgeVar(graph, e, var)))
        var = SUBNET_IPV6
        print var+" : "+str(getEdgeVar(graph, e, var))+" "+str(type(getEdgeVar(graph, e, var)))
        var = BANDWIDTH
        print var+" : "+getEdgeVar(graph, e, var)
        print "----------------------------------------"




