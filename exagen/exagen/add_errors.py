#!/usr/bin/env python

import os
import random
import ipaddress as ia
import networkx as nx
from optparse import OptionParser

import utils as ut
import loadgraphml
import genstatement

# =======================================================================
# ============================ CONFIGURATION ============================
# =======================================================================

EXEC_NETKIT = False
OPEN_STATEMENT = False
LEVELS = [ "EASY", "MEDIUM", "HARD" ]
DEFAULT_LEVEL = "MEDIUM"


# =======================================================================
# ============================== FUNCTIONS ==============================
# =======================================================================

# Utils

def append_error(graph, n, error_str) :
    """"Append error to the *.startup file of the given node."""
    fileObj = open(str(ut.getNodeVar(graph, n, ut.LABEL))+".startup", "a")    
    fileObj.write(error_str+"\n")
    fileObj.close()


def get_random_neighbor_routers(graph) :
    """
        Returns two random neighbor routers.

        :return: Two neighbor routers id's
        :rtype: tuple of int
    """
    n1 = random.choice(graph.nodes())
    n2 = None
    while ut.getNodeVar(graph, n1, ut.DEV_TYPE) != ut.ROUTER :
        n1 = random.choice(graph.nodes())
    neighbors = graph.to_undirected().neighbors(n1)
    random.shuffle(neighbors)
    for n in neighbors :
        if (ut.getNodeVar(graph, n, ut.DEV_TYPE) == ut.ROUTER) :
            n2 = n
            break
    return (n1, n2)


def get_nodes_in_cycles(graph) :
    """Returns a list of nodes being part of a cycle in the graph."""
    cycles = nx.cycle_basis(graph.to_undirected())
    nodes_in_cycles = []
    for c in cycles :
        nodes_in_cycles += c
    return list(set(nodes_in_cycles))


def get_random_router(graph) :
    """Returns a random router of the graph."""
    n = random.choice(graph.nodes())
    while ut.getNodeVar(graph, n, ut.DEV_TYPE) != ut.ROUTER :
        n = random.choice(graph.nodes())
    return n


def get_random_network(graph, subnet_ver) :
    """
        Returns a random network of the graph.

        :param subnet_ver: subnet with IP version
        :type subnet_ver: ut.SUBNET_IPV4 or ut.SUBNET_IPV6
    """
    e = random.choice(graph.edges())
    return ut.getEdgeVar(graph, e, subnet_ver)


# Errors

def down_interface(graph, n) :
    """
        Add an interface down error.

        :Example: 
        ifconfig eth0 down
    """
    iface = random.choice(graph.node[n]['interfaces'].keys())
    error_str = "ifconfig eth"+str(ut.getIfVar(graph, n, iface, ut.IF_NO))+" down"
    append_error(graph, n, error_str)


def disable_ip_forward(graph, n) :
    """
        Add an IP forwarding disabled error.

        :Example:
        sysctl -w net.ipv4.ip_forward=0
        sysctl -w net.ipv6.conf.all.forwarding=0
    """
    error_str = "sysctl -w net.ipv4.ip_forward=0\n"
    error_str += "sysctl -w net.ipv6.conf.all.forwarding=0"
    append_error(graph, n, error_str)


def remove_existing_route(graph, n, net_dest) :
    """
        Remove an existing route error.

        :param net_dest: destination network route to be removed
        :type net_dest: ipaddress.IPv4Network or ipaddress.IPv6Network

        :Example:
        IPv4 : route delete -net ip/subnet
        IPv6 : route -A inet6 delete ip/prefix
    """
    if (net_dest.version == 6) :
        error_str = "route -A inet6 delete "+str(net_dest)
    else :
        error_str = "route delete -net "+str(net_dest)
    append_error(graph, n, error_str)


def replace_route_null(graph, n, net_dest) :
    """
        Replace an existing route by a null route error.

        :param net_dest: destination network route to be replaced
        :type net_dest: ipaddress.IPv4Network or ipaddress.IPv6Network
        
        :Example:
        IPv4 : route add -net ip/subnet gw 127.0.0.1
        IPv6 : route -A inet6 add ip/prefix gw 2001::1 dev lo    
    """
    remove_existing_route(graph, n, net_dest)
    if (net_dest.version == 6) :
        error_str = "route -A inet6 add "+str(net_dest)+" gw ::1 dev lo"
    else :
        error_str = "route add -net "+str(net_dest.network_address)+" netmask "+str(net_dest.netmask)+" gw 127.0.0.1"
    append_error(graph, n, error_str)


def add_black_hole(graph, n, net_dest) :
    """
        Call remove existing route error and one time on two replace by a null route.

        :param net_dest: destination network route to be removed or replaced
        :type net_dest: ipaddress.IPv4Network or ipaddress.IPv6Network
    """ 
    if (random.randint(0, 1) == 0) :
        remove_existing_route(graph, n, net_dest)
    else :
        replace_route_null(graph, n, net_dest)


def add_loop(graph, neighbor_routers, net_dest) :
    """
        Add a loop between the two neighbor routers and for the given network destination.

        :param net_dest: destination network route for loop
        :type net_dest: ipaddress.IPv4Network or ipaddress.IPv6Network
    """
    n1, n2 = neighbor_routers
    
    # Delete correct routes to destination
    remove_existing_route(graph, n1, net_dest)
    remove_existing_route(graph, n2, net_dest)

    # Get linking edge
    e = ut.get_edge_out(graph, n1, n2)

    # Get n1 interface info to this edge
    n1_if_no = graph.node[n1]['interfaces'][e][ut.IF_NO]
    n1_ipv4 = graph.node[n1]['interfaces'][e][ut.IPV4]
    n1_ipv6 = graph.node[n1]['interfaces'][e][ut.IPV6]

    # Get n2 interface info to this edge
    n2_if_no = graph.node[n2]['interfaces'][e][ut.IF_NO]
    n2_ipv4 = graph.node[n2]['interfaces'][e][ut.IPV4]
    n2_ipv6 = graph.node[n2]['interfaces'][e][ut.IPV6]

    # Add looping routes to destination

    if (net_dest.version == 6) :
        error_str = "route -A inet6 add "+str(net_dest)+" gw "+str(n2_ipv6)+" dev eth"+str(n1_if_no)
        append_error(graph, n1, error_str)
        error_str = "route -A inet6 add "+str(net_dest)+" gw "+str(n1_ipv6)+" dev eth"+str(n2_if_no)
        append_error(graph, n2, error_str)
    else :
        error_str = "route add -net "+str(net_dest.network_address)+" netmask "+str(net_dest.netmask)+" gw "+str(n2_ipv4)+" dev eth"+str(n1_if_no)
        append_error(graph, n1, error_str)
        error_str = "route add -net "+str(net_dest.network_address)+" netmask "+str(net_dest.netmask)+" gw "+str(n1_ipv4)+" dev eth"+str(n2_if_no)
        append_error(graph, n2, error_str)


def no_more_shortest_path(graph, n, ip_ver) :
    """
        Change the graph to be no more shortest path.

        :param ip_ver: IP version for the error
        :type ip_ver: 4 or 6
    """
    # TODO: use ip_version constant

    # Choose a distant edge
    e = random.choice(graph.edges())
    while (e in graph.node[n]['interfaces']) :
        e = random.choice(graph.edges())
    n_src, n_tgt = e

    # Get next hops of longer paths (than shortest) to this edge
    s = list(nx.all_simple_paths(graph.to_undirected(), source=n, target=n_src))
    s.sort(key = len, reverse=True) # sort from longest to shortest
    possible_next_hops = []
    for p in s :
        if (len(p) == len(s[0])) :
            possible_next_hops.append(p[1])   

    # Remove nodes connected to this edge
    possible_next_hops = [x for x in possible_next_hops if x not in list(e)]
    random.shuffle(possible_next_hops)

    # There is no possibility for a longer path from this node to this edge
    if len(possible_next_hops) == 0 :
        return no_more_shortest_path(graph, n, ip_ver)

    # Get related info
    next_hop_node = random.choice(possible_next_hops)
    edge_out = ut.get_edge_out(graph, n, next_hop_node)
    if_no = ut.getIfVar(graph, n, edge_out, ut.IF_NO)
    ipv4_gw = ut.getIfVar(graph, next_hop_node, edge_out, ut.IPV4)
    ipv6_gw = ut.getIfVar(graph, next_hop_node, edge_out, ut.IPV6)

    if (ip_ver == 6) :
        # Delete correct route
        e_subnet_ipv6 = ut.getEdgeVar(graph, e, ut.SUBNET_IPV6)
        remove_existing_route(graph, n, e_subnet_ipv6)

        # Add bad route (longer than shortest path)
        error_str = "route -A inet6 add "+str(e_subnet_ipv6)+" gw "+str(ipv6_gw)+" dev eth"+str(if_no)
        append_error(graph, n, error_str)

    else :
        # Delete correct route
        e_subnet_ipv4 = ut.getEdgeVar(graph, e, ut.SUBNET_IPV4)
        remove_existing_route(graph, n, e_subnet_ipv4)

        # Add bad route (longer than shortest path)
        error_str = "route add -net "+str(e_subnet_ipv4.network_address)+" netmask "+str(e_subnet_ipv4.netmask)+" gw "+str(ipv4_gw)+" dev eth"+str(if_no)
        append_error(graph, n, error_str)



# ======================================================================
# ================================ MAIN ================================
# ======================================================================

def main() :

    script_dir = os.path.dirname(os.path.realpath(__file__))

    usage = "Usage: %prog [options] graphml_file"
    parser = OptionParser(usage=usage)

    # Define options
    parser.add_option(
	    '-d',
	    '--directory',
	    dest="lab_dir",
	    help='Netkit lab directory')
    parser.add_option(
	    '-l',
	    '--level',
	    dest="level",
	    help='difficulty level')
    parser.add_option(
	    '-o',
	    '--open',
        action="store_true",
	    dest="open_statement",
        default = OPEN_STATEMENT,
	    help='open statement PDF')
    parser.add_option(
	    '-f',
	    '--file',
	    dest="file",
	    help='save statement with provided filename')
    parser.add_option(
	    '-n',
	    '--netkit',
	    action="store_true",
	    dest="run_netkit",
        default = EXEC_NETKIT,
	    help='launch Netkit lab corresponding to network graphml')

    (options, args) = parser.parse_args()

    # Process options

    if len(args) != 1 :
        parser.error("Error : missing argument.")

    input_abs_filepath = ut.check_input(os.path.abspath(args[0]))

    if (options.file) :
        stmt_abs_filepath = os.path.dirname(input_abs_filepath)+"/"+options.file
    else :
        stmt_abs_filepath = input_abs_filepath[:-8]+"_statement" 

    if (options.lab_dir) :
        lab_dir = os.path.dirname(input_abs_filepath) +"/"+ options.lab_dir
    else :
        lab_dir = os.path.dirname(input_abs_filepath) + "/lab_" + os.path.basename(input_abs_filepath)[:-8]

    if (not os.path.isdir(lab_dir)) :
        os.system("mkdir -p "+lab_dir)

    if (options.level) :
        if (options.level not in LEVELS) :
            ut.error("Bad difficulty level. Existing levels : "+', '.join(LEVELS)+".")
    else :
        options.level = DEFAULT_LEVEL

    graph = loadgraphml.load_graphml(input_abs_filepath)

    os.chdir(lab_dir)

    # Add errors according to difficulty level

    if (options.level == "EASY") :
        disable_ip_forward(graph, get_random_router(graph))
        down_interface(graph, get_random_router(graph))
        add_black_hole(graph, get_random_router(graph), get_random_network(graph, ut.SUBNET_IPV4))

    if (options.level == "MEDIUM") :
        disable_ip_forward(graph, get_random_router(graph))
        down_interface(graph, get_random_router(graph))
        add_black_hole(graph, get_random_router(graph), get_random_network(graph, ut.SUBNET_IPV4))
        n_in_cycles = get_nodes_in_cycles(graph)
        if (n_in_cycles) :
            no_more_shortest_path(graph, random.choice(n_in_cycles), 4)

    if (options.level == "HARD") :
        disable_ip_forward(graph, get_random_router(graph))
        down_interface(graph, get_random_router(graph))
        add_black_hole(graph, get_random_router(graph), get_random_network(graph, ut.SUBNET_IPV4))
        add_black_hole(graph, get_random_router(graph), get_random_network(graph, ut.SUBNET_IPV4))
        n_in_cycles = get_nodes_in_cycles(graph)
        if (n_in_cycles) :
            no_more_shortest_path(graph, random.choice(n_in_cycles), 4)
        add_loop(graph, get_random_neighbor_routers(graph), get_random_network(graph, ut.SUBNET_IPV4))

    os.chdir(os.path.dirname(input_abs_filepath))

    genstatement.gen_statement(graph, script_dir, stmt_abs_filepath)

    # Open statement if needed
    if (options.open_statement) :
        os.system("xdg-open "+stmt_abs_filepath+".pdf")  

    # Run Netkit lab if needed
    if (options.run_netkit) :
        ut.launch_netkit(lab_dir)



if __name__ == "__main__":
    main()



