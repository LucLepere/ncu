#!/usr/bin/env python

import os
import random
import networkx as nx
from optparse import OptionParser

import utils as ut
import gengraphml


# =======================================================================
# ============================ CONFIGURATION ============================
# =======================================================================

ROUTERS_MIN_DEFAULT = 4
ROUTERS_MAX_DEFAULT = 6
FILENAME_DEFAULT = "network.graphml"
DEFAULT_DIR = ""
EXEC_YED = False


# =======================================================================
# ============================== FUNCTIONS ==============================
# =======================================================================

def generate_full_mesh(nb_routers) :
    """
        Generate a NetworkX full mesh graph.

        .. note:: destined to disappear
    """
    # TODO: use nx.complete_graph(n)
    nb_links = nb_routers*(nb_routers-1)/2 # max (full mesh graph)
    return nx.gnm_random_graph(nb_routers, nb_links) # random with max = full mesh


def relabel_nodes(graph) :
    """Relabel nodes to 'ni' (n0, n1, ...)."""
    labels_dict = {}
    for n in graph.nodes() :
        labels_dict[n] = "n"+str(n)
    return nx.relabel_nodes(graph, labels_dict)


def specialize_nodes_in_routers(graph) :
    """Specialize nodes in routers (set dev_type and appropriate label)."""
    for n in graph.nodes() :
        graph.node[n][ut.DEV_TYPE] = ut.ROUTER
        graph.node[n][ut.LABEL] = "router"+str(int(n[1:])+1)
    return graph


def remove_random_links(graph) :
    """Remove a random number of edges of the graph but keeping it connected."""
    nb_routers = graph.number_of_nodes()
    curr_nb_links = graph.number_of_edges()

    # Links number
    min_links = nb_routers-1              # min for a connected graph
    nb_links = random.randint(min_links, curr_nb_links)

    # Remove random number of links but keeping the graph connected
    nb_links_to_remove = curr_nb_links - nb_links

    for e in graph.edges() :
        graph.remove_edge(*e)
        if (not nx.is_connected(graph)) :
            graph.add_edge(*e)
        else :
            nb_links_to_remove -= 1
            if (nb_links_to_remove == 0) :
                break;
    return graph


def add_host_each_router(graph) :
    """Add and link one host node to each router node."""
    nb_routers = graph.number_of_nodes()
    host_i = 1
    for i in range(nb_routers, 2*nb_routers) :
        n = "n"+str(i)
        graph.add_node(n)
        graph.node[n][ut.DEV_TYPE] = ut.HOST
        graph.node[n][ut.LABEL] = "host"+str(host_i)
        graph.add_edge(n, "n"+str(host_i-1))
        host_i += 1
    return graph


def attribute_edge_ids(graph) :
    """Attribute edge id's (e0, e1, ...)."""
    i = 0
    for n_src, n_tgt in graph.edges() :
        graph.edge[n_src][n_tgt][ut.ID] = "e"+str(i)
        i += 1 
    return graph


def position_nodes(graph) :
    """"Give a layout to the graph and record yEd-scaled positions."""
    pos = nx.spring_layout(graph)
    # Put positions in graph nodes
    for n in graph.nodes() :
        graph.node[n][ut.POS_X] = pos[n][0] * ut.YED_X_SCALE
        graph.node[n][ut.POS_Y] = (1-pos[n][1]) * ut.YED_Y_SCALE
    return graph



# ======================================================================
# ================================ MAIN ================================
# ======================================================================

def main() :

    nb_routers  = None
    filename    = FILENAME_DEFAULT

    usage = "Usage: %prog [options]"
    parser = OptionParser(usage=usage)

    # Define options
    parser.add_option(
	    '-r',
	    '--routers',
	    dest="routers",
	    help='number of routers, exact number (e.g. : 5) or range (e.g. : 4-7)')
    parser.add_option(
	    '-f',
	    '--file',
	    dest="file",
	    help='save with provided filename')
    parser.add_option(
	    '-y',
	    '--yEd',
	    action="store_true",
	    dest="open_yed",
        default = EXEC_YED,
	    help='open yEd with created network graphml')

    (options, args) = parser.parse_args()

    # Process options

    if (options.routers) :
        nb_routers = ut.get_nb(options.routers, "routers")
    else :
        nb_routers = random.randint(ROUTERS_MIN_DEFAULT, ROUTERS_MAX_DEFAULT)

    if (options.file) :
        filename = options.file

    graph = generate_full_mesh(nb_routers)
    graph = relabel_nodes(graph)
    graph = specialize_nodes_in_routers(graph)
    graph = remove_random_links(graph)
    graph = add_host_each_router(graph)
    graph = attribute_edge_ids(graph)
    graph = ut.attribute_edge_labels(graph)
    graph = position_nodes(graph)

    if (DEFAULT_DIR) :
        filename = DEFAULT_DIR+"/"+filename

    abs_filepath = ut.get_filepath(filename)
    ut.save_file(abs_filepath, gengraphml.generate_graphml(graph))

    # Open file in yEd if needed
    if (options.open_yed) :
        ut.launch_yed(abs_filepath)


if __name__ == "__main__":
    main()



