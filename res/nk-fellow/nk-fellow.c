#include <stdio.h>
#include <signal.h>
#include <syslog.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/inotify.h>


/* Please note that this program is based from "A Simple Daemon in C" article (http://www.4pmp.com/2009/12/a-simple-daemon-in-c/) */


#define DAEMON_NAME     "nk-fellow"

// Launch scripts names
#define LG_LAUNCH_SH    "launch_lg.sh"
#define WS_LAUNCH_SH    "launch_ws.sh"

// inotify relative
#define EVENT_SIZE      (sizeof (struct inotify_event))
#define EVENT_BUF_LEN   (1024 * (EVENT_SIZE + 16))


void daemonShutdown();
void signal_handler(int sig);
void daemonize(char *rundir, char *pidfile);

int pidFilehandle;
int fd, wd[1];

void signal_handler(int sig)
{
    switch(sig)
    {
        case SIGHUP:
            syslog(LOG_WARNING, "Received SIGHUP signal.");
            break;
        case SIGINT:
        case SIGTERM:
            syslog(LOG_INFO, "Daemon exiting");
            daemonShutdown();
            exit(EXIT_SUCCESS);
            break;
        default:
            syslog(LOG_WARNING, "Unhandled signal %s", strsignal(sig));
            break;
    }
}

void daemonShutdown()
{
    // Remove directories from watchlist
    inotify_rm_watch(fd, wd[0]);
    inotify_rm_watch(fd, wd[1]);

    // Close inotify instance
    close(fd);

    close(pidFilehandle);
}

void daemonize(char *rundir, char *pidfile)
{
    int pid, sid, i;
    char str[10];
    struct sigaction newSigAction;
    sigset_t newSigSet;

    /* Check if parent process id is set */
    if (getppid() == 1)
    {
        /* PPID exists, therefore we are already a daemon */
        return;
    }

    /* Set signal mask - signals we want to block */
    sigemptyset(&newSigSet);
    sigaddset(&newSigSet, SIGCHLD);  /* ignore child - i.e. we don't need to wait for it */
    sigaddset(&newSigSet, SIGTSTP);  /* ignore Tty stop signals */
    sigaddset(&newSigSet, SIGTTOU);  /* ignore Tty background writes */
    sigaddset(&newSigSet, SIGTTIN);  /* ignore Tty background reads */
    sigprocmask(SIG_BLOCK, &newSigSet, NULL);   /* Block the above specified signals */

    /* Set up a signal handler */
    newSigAction.sa_handler = signal_handler;
    sigemptyset(&newSigAction.sa_mask);
    newSigAction.sa_flags = 0;

    /* Signals to handle */
    sigaction(SIGHUP, &newSigAction, NULL);     /* catch hangup signal */
    sigaction(SIGTERM, &newSigAction, NULL);    /* catch term signal */
    sigaction(SIGINT, &newSigAction, NULL);     /* catch interrupt signal */

    /* Fork */
    pid = fork();

    if (pid < 0)
    {
        /* Could not fork */
        exit(EXIT_FAILURE);
    }

    if (pid > 0)
    {
        /* Child created ok, so exit parent process */
        printf("Child process created: %d\n", pid);
        exit(EXIT_SUCCESS);
    }

    /* Child continues */

    umask(027); /* Set file permissions 750 */

    /* Get a new process group */
    sid = setsid();

    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    /* close all descriptors */
    for (i = getdtablesize(); i >= 0; --i)
    {
        close(i);
    }

    /* Route I/O connections */

    /* Open STDIN */
    i = open("/dev/null", O_RDWR);

    /* STDOUT */
    dup(i);

    /* STDERR */
    dup(i);

    chdir(rundir); /* change running directory */

    
    // Ensure only one copy
    pidFilehandle = open(pidfile, O_RDWR|O_CREAT, 0600);

    if (pidFilehandle == -1 )
    {
        // Couldn't open lock file
        syslog(LOG_ERR, "Could not open PID lock file %s, exiting", pidfile);
        exit(EXIT_FAILURE);
    }

    // Try to lock file
    if (lockf(pidFilehandle,F_TLOCK,0) == -1)
    {
        // Couldn't get lock on lock file
        syslog(LOG_ERR, "Could not lock PID lock file %s, exiting", pidfile);
        exit(EXIT_FAILURE);
    }

    // Get and format PID
    sprintf(str,"%d\n",getpid());

    // write pid to lockfile
    write(pidFilehandle, str, strlen(str));
    
}

int main()
{
    /* Logging */
    setlogmask(LOG_UPTO(LOG_INFO));
    //setlogmask(LOG_UPTO(LOG_DEBUG));
    openlog(DAEMON_NAME, LOG_CONS | LOG_PERROR, LOG_USER);

    syslog(LOG_INFO, "Daemon starting up");

    /* Deamonize */
    daemonize("/tmp/", "/tmp/nk-fellow.pid");

    // Init vars
	int length, i;
    char buffer[EVENT_BUF_LEN];

    // Create inotify instance
    fd = inotify_init();
    if (fd < 0)
    {
		/* Log the failure */
		syslog (LOG_ERR, "ERROR : inotify_init failed.");
        exit(EXIT_FAILURE);
    }

    // Add LiveGraph directory to watchlist
    wd[0] = inotify_add_watch(fd, getenv("NKF_LG_DATA"), IN_CREATE);
	if (wd[0] < 0)
	{
		/* Log the failure */
		//syslog (LOG_ERR, "ERROR : inotify_add_watch failed (%s %s).", strerror(errno), NKF_LG_DATA);
        syslog (LOG_ERR, "ERROR : inotify_add_watch failed (%s).", strerror(errno));
        exit(EXIT_FAILURE);
	}

    // Add Wireshark directory to watchlist
    wd[1] = inotify_add_watch(fd, getenv("NKF_WS_DATA"), IN_CREATE);
	if (wd[1] < 0)
	{
		/* Log the failure */
		syslog (LOG_ERR, "ERROR : inotify_add_watch failed (%s).", strerror(errno));
        exit(EXIT_FAILURE);
	}

    syslog(LOG_INFO, "Daemon running");

    while (1)
    {
        i = 0;

        // Read to determine event changes on directory (actually this read blocks until the change event occurs)
        length = read(fd, buffer, EVENT_BUF_LEN);
        if (length < 0)
        {
			/* Log the failure */
			syslog (LOG_ERR, "ERROR : read failed.");
        }

        // Read returns the list of change events
        while (i < length)
        {
            struct inotify_event *event = (struct inotify_event *) &buffer[i];
            if (event->len)
            {
                if (!(event->mask & IN_ISDIR))
                {
                    char cmd[256];

                    // File in LiveGraph directory
                    if (event->wd == wd[0])
                    {
                        syslog (LOG_INFO, "LiveGraph new file %s created.", event->name);    

                        // Call dedicated script
                        sprintf(cmd, "%s/%s %s", getenv("NKF_HOME"), LG_LAUNCH_SH, getenv("LG_DIR"));

                    }

                    // File in Wireshark directory
                    if (event->wd == wd[1]) 
                    {
                        syslog (LOG_INFO, "Wireshark new file %s created.", event->name);

                        // Call dedicated script
                        sprintf(cmd, "%s/%s %s %s", getenv("NKF_HOME"), WS_LAUNCH_SH, event->name, getenv("NKF_WS_DATA"));
				        
                    }
                    syslog (LOG_DEBUG, "Command run : %s", cmd);		
                    int ret = system(cmd);
			        syslog (LOG_DEBUG, "System() call result : %d", ret);
                }   
            }
            i += EVENT_SIZE + event->len;
        }
    }
}

