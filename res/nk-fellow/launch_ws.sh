#/bin/bash

FILE=$1
NKF_WS_DATA=$2

rm /tmp/ws_pipe
mkfifo /tmp/ws_pipe
tail -f $NKF_WS_DATA/$FILE > /tmp/ws_pipe &
wireshark -k -i /tmp/ws_pipe &

