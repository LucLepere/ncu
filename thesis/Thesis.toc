\contentsline {chapter}{\numberline {1}Introduction}{5}
\contentsline {chapter}{\numberline {2}Background material}{7}
\contentsline {section}{\numberline {2.1}Netkit}{7}
\contentsline {section}{\numberline {2.2}Wireshark}{8}
\contentsline {chapter}{\numberline {3}Related work}{10}
\contentsline {section}{\numberline {3.1}AutoNetkit}{10}
\contentsline {section}{\numberline {3.2}tcpsnoop}{11}
\contentsline {section}{\numberline {3.3}Click}{11}
\contentsline {chapter}{\numberline {4}Solution}{15}
\contentsline {section}{\numberline {4.1}Microscopic TCP behavior}{16}
\contentsline {subsection}{\numberline {4.1.1}Managing a queue}{16}
\contentsline {subsection}{\numberline {4.1.2}Click modification}{16}
\contentsline {subsection}{\numberline {4.1.3}Click compilation}{19}
\contentsline {subsection}{\numberline {4.1.4}Click communication}{20}
\contentsline {subsection}{\numberline {4.1.5}Traffic observation}{20}
\contentsline {subsection}{\numberline {4.1.6}Automated launch}{22}
\contentsline {section}{\numberline {4.2}Macroscopic TCP behavior}{24}
\contentsline {subsection}{\numberline {4.2.1}Topologies with links of different bandwidths}{24}
\contentsline {subsection}{\numberline {4.2.2}AutoNetkit architecture}{25}
\contentsline {subsection}{\numberline {4.2.3}AutoNetkit modification for bandwidth}{25}
\contentsline {subsection}{\numberline {4.2.4}Traffic shaping}{25}
\contentsline {subsection}{\numberline {4.2.5}Network variables}{26}
\contentsline {subsection}{\numberline {4.2.6}Traffic generation}{27}
\contentsline {subsection}{\numberline {4.2.7}The TCP\_INFO option}{27}
\contentsline {subsection}{\numberline {4.2.8}Graph representation}{31}
\contentsline {subsection}{\numberline {4.2.9}LiveGraph presentation}{31}
\contentsline {subsection}{\numberline {4.2.10}LiveGraph exploitation}{32}
\contentsline {subsection}{\numberline {4.2.11}Some interesting examples}{36}
\contentsline {section}{\numberline {4.3}Exams-like exercises generator : ``exagen''}{37}
\contentsline {subsection}{\numberline {4.3.1}Present situation}{37}
\contentsline {subsection}{\numberline {4.3.2}Idea}{37}
\contentsline {subsection}{\numberline {4.3.3}Analysis}{37}
\contentsline {subsection}{\numberline {4.3.4}Workflow}{38}
\contentsline {subsection}{\numberline {4.3.5}Modules}{40}
\contentsline {subsubsection}{\numberline {4.3.5.1}create\_topo}{40}
\contentsline {subsubsection}{\numberline {4.3.5.2}gengraphml}{40}
\contentsline {subsubsection}{\numberline {4.3.5.3}config\_ip}{40}
\contentsline {subsubsection}{\numberline {4.3.5.4}create\_lab}{42}
\contentsline {subsubsection}{\numberline {4.3.5.5}add\_errors}{42}
\contentsline {subsubsection}{\numberline {4.3.5.6}genstatement}{43}
\contentsline {subsubsection}{\numberline {4.3.5.7}Others}{43}
\contentsline {subsection}{\numberline {4.3.6}Checks}{43}
\contentsline {subsection}{\numberline {4.3.7}Packaging}{44}
\contentsline {subsection}{\numberline {4.3.8}Summary}{44}
\contentsline {subsection}{\numberline {4.3.9}Future work}{44}
\contentsline {subsection}{\numberline {4.3.10}Threats}{44}
\contentsline {chapter}{\numberline {5}Future work}{46}
\contentsline {chapter}{\numberline {6}Conclusion}{48}
\vspace *{\baselineskip }
\contentsline {chapter}{Bibliography}{49}
\contentsline {chapter}{\numberline {A}Listing of sources}{53}
