struct tcp_info
{
	__u8    tcpi_state;
	__u8    tcpi_ca_state;
	__u8    tcpi_retransmits;
	__u8    tcpi_probes;
	__u8    tcpi_backoff;
	__u8    tcpi_options;
	__u8    tcpi_snd_wscale : 4, tcpi_rcv_wscale : 4;

	__u32   tcpi_rto;
	__u32   tcpi_ato;
	__u32   tcpi_snd_mss;
	__u32   tcpi_rcv_mss;

	__u32   tcpi_unacked;
	__u32   tcpi_sacked;
	__u32   tcpi_lost;
	__u32   tcpi_retrans;
	__u32   tcpi_fackets;

	/* Times. */
	__u32   tcpi_last_data_sent;
	__u32   tcpi_last_ack_sent;     /* Not remembered, sorry. */
	__u32   tcpi_last_data_recv;
	__u32   tcpi_last_ack_recv;

	/* Metrics. */
	__u32   tcpi_pmtu;
	__u32   tcpi_rcv_ssthresh;
	__u32   tcpi_rtt;
	__u32   tcpi_rttvar;
	__u32   tcpi_snd_ssthresh;
	__u32   tcpi_snd_cwnd;
	__u32   tcpi_advmss;
	__u32   tcpi_reordering;

	__u32   tcpi_rcv_rtt;
	__u32   tcpi_rcv_space;

	__u32   tcpi_total_retrans;
};