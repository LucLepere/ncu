#!/bin/bash


# Configuration
RECEIVER_IP=192.168.0.2


echo ""
echo "Note : tcpcomm4 receiver must be running on $RECEIVER_IP"
echo ""
echo "Configuration :"
echo "tcpcomm_running_time = $1"
sysctl net.ipv4.tcp_wmem
sysctl net.ipv4.tcp_rmem
echo ""
echo "Run the test..."
echo ""
echo ""

array=( bic cubic highspeed htcp hybla illinois lp reno scalable vegas veno westwood yeah )

cd /hosthome/nk_ncu/tcpcomm4

for i in "${array[@]}"
do
    /hosthome/nk_ncu/easychange.py -c $i
    sysctl net.ipv4.tcp_congestion_control
    timeout $1 ./sender -v 2+26+27 -f $2_$i $RECEIVER_IP
    sleep 2
done


