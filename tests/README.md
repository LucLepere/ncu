
Please note that these tests need the "coreutils" package to be installed.
In order to do this, use a Netkit tap and add "nameserver 8.8.8.8" in /etc/resolv.conf. Then, just "apt-get install coreutils".


These tests are designed to be used on the pc1_to_pc2_dualstack_lab.
Run tcpcomm receiver on pc2.
Run test scripts on pc1.


